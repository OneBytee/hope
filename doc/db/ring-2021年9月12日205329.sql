/*
 Navicat Premium Data Transfer

 Source Server         : mysql-local
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : ring

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 12/09/2021 20:48:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `address_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地址编号',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编号',
  `address_person` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人',
  `address_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `address_province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '省份',
  `address_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '城市',
  `address_area` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区县',
  `address_detaile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '信息地址',
  `address_check` int NOT NULL COMMENT '是否设为默认地址 1 默认0不默认',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`address_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户地址管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('HJW12341508299017175', 'HJW1234', '王大虎', '12312312345', '山西省', '太原市', '小店区', '梧桐街126号', 0, '2017-10-18 11:58:56', '2017-10-18 19:53:45');
INSERT INTO `address` VALUES ('HJW12341508327506045', 'HJW1234', '王大虎', '23434567890', '内蒙古自治区', '呼和浩特市', '新城区', '莲花街123号', 0, '2017-10-18 19:53:45', '2017-10-18 19:54:44');
INSERT INTO `address` VALUES ('HJW12341508327565135', 'HJW1234', '王大虎', '23423423415', '北京市', '北京市', '朝阳区', '雪松路123号', 0, '2017-10-18 19:54:44', '2017-10-18 19:55:16');
INSERT INTO `address` VALUES ('HJW12341508327597769', 'HJW1234', '王大虎', '23423456765', '辽宁省', '沈阳市', '和平区', '腊梅路123号', 0, '2017-10-18 19:55:16', '2017-10-18 19:56:54');
INSERT INTO `address` VALUES ('HJW12341508327695478', 'HJW1234', '王大虎', '12312345645', '黑龙江省', '哈尔滨市', '道里区', '科学大道123号', 0, '2017-10-18 19:56:54', '2017-10-18 19:57:21');
INSERT INTO `address` VALUES ('HJW12341508327722113', 'HJW1234', '王大虎', '12345567890', '安徽省', '合肥市', '瑶海区', '翠竹街123号', 0, '2017-10-18 19:57:21', '2017-10-18 20:00:13');
INSERT INTO `address` VALUES ('HJW12341508327895005', 'HJW1234', '王大虎', '12345665432', '河北省', '石家庄市', '长安区', '航海路123号', 0, '2017-10-18 20:00:14', '2017-10-18 20:01:41');
INSERT INTO `address` VALUES ('HJW12341508327982134', 'HJW1234', '王大虎', '12312312345', '吉林省', '长春市', '南关区', '农业路123号', 0, '2017-10-18 20:01:41', '2017-10-19 09:38:13');
INSERT INTO `address` VALUES ('HJW12341508382763345', 'HJW1234', '王大虎', '12312312345', '上海市', '上海市', '黄浦区', '春藤路120号', 0, '2017-10-19 11:14:43', '2017-10-19 11:15:05');
INSERT INTO `address` VALUES ('HJW12341508385674606', 'HJW1234', '王大虎', '12312312345', '浙江省', '杭州市', '上城区', '金水路134号', 0, '2017-10-19 12:03:14', '2017-10-19 14:14:13');
INSERT INTO `address` VALUES ('HJW12341508392177689', 'HJW1234', '王大虎', '123123345577', '青海省', '西宁市', '城东区', '复兴路196号', 0, '2017-10-19 13:51:37', '2017-10-19 13:56:06');
INSERT INTO `address` VALUES ('HJW12341508392446940', 'HJW1234', '王大虎', '12312312345', '河南省', '郑州市', '中原区', '云和数据3号楼', 1, '2017-10-19 13:56:06', '2017-10-19 14:47:27');

-- ----------------------------
-- Table structure for appointment_order
-- ----------------------------
DROP TABLE IF EXISTS `appointment_order`;
CREATE TABLE `appointment_order`  (
  `appointment_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '预约订单编号',
  `site_emp_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务员编号',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户编号',
  `cargo_type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '货物类型编号',
  `appointment_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预约人姓名',
  `appointment_tel` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预约人联系方式',
  `appointment_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预约详细地址',
  `appointment_province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预约地址省份',
  `appointment_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预约地址市',
  `appointment_area` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预约地址区域',
  `appointment_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态',
  `appointment_pare_time` datetime NULL DEFAULT NULL COMMENT '预约时间',
  PRIMARY KEY (`appointment_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户预约订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of appointment_order
-- ----------------------------
INSERT INTO `appointment_order` VALUES ('1508813125296', NULL, NULL, '家庭用品', '刘松山', '15890197161', '电子商务产业园', '河南', '郑州市', '中原区', '2017-10-24 10:46:14', '2017-10-24 10:46:14', '来一份炒面！！', NULL, '2017-10-24 10:45:10');
INSERT INTO `appointment_order` VALUES ('1508836167576', NULL, '67576', '2', '武志成', '11011011011', '武大村', '河南省', '周口市', '淮阳县', '2017-10-24 17:10:16', '2017-10-24 17:10:16', '东南角', '待预约', '2017-10-24 17:09:04');
INSERT INTO `appointment_order` VALUES ('1508836444277', NULL, '44277', '3', 'login', '12012012022', '大王村', '山西省', '长治市', '梧桐县', '2017-10-24 17:14:53', '2017-10-24 17:14:53', '哥哥', '待预约', '2017-10-24 17:13:55');

-- ----------------------------
-- Table structure for batch_job_execution
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution`;
CREATE TABLE `batch_job_execution`  (
  `JOB_EXECUTION_ID` bigint NOT NULL,
  `VERSION` bigint NULL DEFAULT NULL,
  `JOB_INSTANCE_ID` bigint NOT NULL,
  `CREATE_TIME` datetime NOT NULL,
  `START_TIME` datetime NULL DEFAULT NULL,
  `END_TIME` datetime NULL DEFAULT NULL,
  `STATUS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_CODE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LAST_UPDATED` datetime NULL DEFAULT NULL,
  `JOB_CONFIGURATION_LOCATION` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`) USING BTREE,
  INDEX `JOB_INST_EXEC_FK`(`JOB_INSTANCE_ID`) USING BTREE,
  CONSTRAINT `JOB_INST_EXEC_FK` FOREIGN KEY (`JOB_INSTANCE_ID`) REFERENCES `batch_job_instance` (`JOB_INSTANCE_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_execution
-- ----------------------------

-- ----------------------------
-- Table structure for batch_job_execution_context
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_context`;
CREATE TABLE `batch_job_execution_context`  (
  `JOB_EXECUTION_ID` bigint NOT NULL,
  `SHORT_CONTEXT` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SERIALIZED_CONTEXT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_CTX_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_execution_context
-- ----------------------------

-- ----------------------------
-- Table structure for batch_job_execution_params
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_params`;
CREATE TABLE `batch_job_execution_params`  (
  `JOB_EXECUTION_ID` bigint NOT NULL,
  `TYPE_CD` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `KEY_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STRING_VAL` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DATE_VAL` datetime NULL DEFAULT NULL,
  `LONG_VAL` bigint NULL DEFAULT NULL,
  `DOUBLE_VAL` double NULL DEFAULT NULL,
  `IDENTIFYING` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  INDEX `JOB_EXEC_PARAMS_FK`(`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_PARAMS_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_execution_params
-- ----------------------------

-- ----------------------------
-- Table structure for batch_job_execution_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_execution_seq`;
CREATE TABLE `batch_job_execution_seq`  (
  `ID` bigint NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_execution_seq
-- ----------------------------
INSERT INTO `batch_job_execution_seq` VALUES (0, '0');

-- ----------------------------
-- Table structure for batch_job_instance
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_instance`;
CREATE TABLE `batch_job_instance`  (
  `JOB_INSTANCE_ID` bigint NOT NULL,
  `VERSION` bigint NULL DEFAULT NULL,
  `JOB_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_KEY` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`JOB_INSTANCE_ID`) USING BTREE,
  UNIQUE INDEX `JOB_INST_UN`(`JOB_NAME`, `JOB_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_instance
-- ----------------------------

-- ----------------------------
-- Table structure for batch_job_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_job_seq`;
CREATE TABLE `batch_job_seq`  (
  `ID` bigint NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_job_seq
-- ----------------------------
INSERT INTO `batch_job_seq` VALUES (0, '0');

-- ----------------------------
-- Table structure for batch_step_execution
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution`;
CREATE TABLE `batch_step_execution`  (
  `STEP_EXECUTION_ID` bigint NOT NULL,
  `VERSION` bigint NOT NULL,
  `STEP_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_EXECUTION_ID` bigint NOT NULL,
  `START_TIME` datetime NOT NULL,
  `END_TIME` datetime NULL DEFAULT NULL,
  `STATUS` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `COMMIT_COUNT` bigint NULL DEFAULT NULL,
  `READ_COUNT` bigint NULL DEFAULT NULL,
  `FILTER_COUNT` bigint NULL DEFAULT NULL,
  `WRITE_COUNT` bigint NULL DEFAULT NULL,
  `READ_SKIP_COUNT` bigint NULL DEFAULT NULL,
  `WRITE_SKIP_COUNT` bigint NULL DEFAULT NULL,
  `PROCESS_SKIP_COUNT` bigint NULL DEFAULT NULL,
  `ROLLBACK_COUNT` bigint NULL DEFAULT NULL,
  `EXIT_CODE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EXIT_MESSAGE` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LAST_UPDATED` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`) USING BTREE,
  INDEX `JOB_EXEC_STEP_FK`(`JOB_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `JOB_EXEC_STEP_FK` FOREIGN KEY (`JOB_EXECUTION_ID`) REFERENCES `batch_job_execution` (`JOB_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_step_execution
-- ----------------------------

-- ----------------------------
-- Table structure for batch_step_execution_context
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution_context`;
CREATE TABLE `batch_step_execution_context`  (
  `STEP_EXECUTION_ID` bigint NOT NULL,
  `SHORT_CONTEXT` varchar(2500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SERIALIZED_CONTEXT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`STEP_EXECUTION_ID`) USING BTREE,
  CONSTRAINT `STEP_EXEC_CTX_FK` FOREIGN KEY (`STEP_EXECUTION_ID`) REFERENCES `batch_step_execution` (`STEP_EXECUTION_ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_step_execution_context
-- ----------------------------

-- ----------------------------
-- Table structure for batch_step_execution_seq
-- ----------------------------
DROP TABLE IF EXISTS `batch_step_execution_seq`;
CREATE TABLE `batch_step_execution_seq`  (
  `ID` bigint NOT NULL,
  `UNIQUE_KEY` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `UNIQUE_KEY_UN`(`UNIQUE_KEY`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of batch_step_execution_seq
-- ----------------------------
INSERT INTO `batch_step_execution_seq` VALUES (0, '0');

-- ----------------------------
-- Table structure for cargo
-- ----------------------------
DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo`  (
  `cargo_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物编号',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编号',
  `cargo_type_id` int UNSIGNED NOT NULL COMMENT '货物类型编号',
  `cargo_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物名称',
  `cargo_weight` decimal(5, 2) UNSIGNED NOT NULL COMMENT '货物重量',
  `cargo_value` decimal(7, 2) UNSIGNED NOT NULL COMMENT '货物价值',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`cargo_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '物品表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cargo
-- ----------------------------

-- ----------------------------
-- Table structure for cargo_path
-- ----------------------------
DROP TABLE IF EXISTS `cargo_path`;
CREATE TABLE `cargo_path`  (
  `cargo_path_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物路径编号',
  `cargo_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '货物编号',
  `cargo_path_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径站名',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`cargo_path_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '货物路径表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cargo_path
-- ----------------------------

-- ----------------------------
-- Table structure for cargo_type
-- ----------------------------
DROP TABLE IF EXISTS `cargo_type`;
CREATE TABLE `cargo_type`  (
  `cargo_type_id` int NOT NULL COMMENT '货物类型编号',
  `cargo_type_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物类型名称',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`cargo_type_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '货物分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cargo_type
-- ----------------------------
INSERT INTO `cargo_type` VALUES (1, '家用电器', '2017-10-14 11:36:27', '2017-10-14 11:36:27');
INSERT INTO `cargo_type` VALUES (2, '家庭用品', '2017-10-14 11:36:27', '2017-10-14 11:36:27');
INSERT INTO `cargo_type` VALUES (3, '数码产品', '2017-10-14 11:36:27', '2017-10-14 11:36:27');
INSERT INTO `cargo_type` VALUES (4, '绝密文件', '2017-10-14 11:36:27', '2017-10-14 11:36:27');

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `city_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '城市ID',
  `city_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '城市名称',
  `city_person` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人',
  `emp_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人工号',
  `city_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `city_province` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属省份',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`city_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '城市信息管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('c1302', '洛阳', '王大虎', '', '12312312345', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1303', '开封', '王大虎', '', '12345678909', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1304', '鹤壁', '王大虎', '', '12312312345', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1305', '信阳', '王大虎', '', '12345678765', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1306', '南阳', '王大虎', '', '12345654321', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1307', '许昌', '王大虎', '', '12323434567', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1308', '漯河', '王大虎', '', '12345665432', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1309', '银川', '王大虎', '', '12345676532', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1310', '南京', '王大虎', '', '12345678908', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1311', '北京', '王大虎', '', '23412312890', '河南', '2017-10-14 09:55:09', '2017-10-19 20:24:15');
INSERT INTO `city` VALUES ('c1312', '长沙', '王大虎', '', '12312312345', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1313', '长春', '王大虎', '', '12312312345', '吉林', '2017-10-18 20:14:49', '2017-10-18 20:14:49');
INSERT INTO `city` VALUES ('c1314', '武汉', '王大虎', NULL, '12312345678', '湖北', '2017-10-19 20:27:35', '2017-10-19 20:27:35');
INSERT INTO `city` VALUES ('c1315', '福州', '王大虎', NULL, '23456789098', '福建', '2017-10-19 20:28:21', '2017-10-19 20:28:21');
INSERT INTO `city` VALUES ('c1316', '银川市', '王大虎', NULL, '12312312345', '宁夏省', '2017-10-24 16:30:06', '2017-10-24 16:30:06');
INSERT INTO `city` VALUES ('c1317', '兰州市', '王大辉', NULL, '12334534567', '甘肃省', '2017-10-24 16:37:38', '2017-10-24 16:37:38');
INSERT INTO `city` VALUES ('c1318', '杭州市', '王大辉', NULL, '12365458545', '浙江省', '2017-10-24 16:46:20', '2017-10-24 16:46:20');
INSERT INTO `city` VALUES ('c1319', '南京市', '王大辉', NULL, '15454525454', '江苏省', '2017-10-24 16:48:21', '2017-10-24 16:48:21');
INSERT INTO `city` VALUES ('c1320', '西安市', '王大辉', NULL, '13654525456', '陕西省', '2017-10-24 16:51:42', '2017-10-24 16:51:42');

-- ----------------------------
-- Table structure for claim_order
-- ----------------------------
DROP TABLE IF EXISTS `claim_order`;
CREATE TABLE `claim_order`  (
  `claim_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '取货订单编号',
  `site_emp_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务员编号',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编号',
  `user_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '交易人姓名',
  `cargo_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物编号',
  `payid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单交易号',
  `contacts_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人姓名',
  `contacts_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `contacts_addr` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系地址',
  `amount` decimal(5, 2) NOT NULL COMMENT '订单金额',
  `paytype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支付方式',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`claim_id`) USING BTREE,
  UNIQUE INDEX `io_order_payid`(`payid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '取货订单表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of claim_order
-- ----------------------------

-- ----------------------------
-- Table structure for code
-- ----------------------------
DROP TABLE IF EXISTS `code`;
CREATE TABLE `code`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '注册手机号',
  `code` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '验证码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '手机注册验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of code
-- ----------------------------
INSERT INTO `code` VALUES (35, '18237101977', '557978');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon`  (
  `coupon_id` int NOT NULL AUTO_INCREMENT COMMENT '卡券编号',
  `coupon_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '卡券类型',
  `coupon_number` int NOT NULL COMMENT '卡券发布数量',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`coupon_id`) USING BTREE,
  UNIQUE INDEX `Index 2`(`coupon_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '卡券表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES (1, '月卡', 100, '2017-10-14 11:38:05', '2017-10-14 11:38:05');
INSERT INTO `coupon` VALUES (2, '月卡', 100, '2017-10-14 11:38:05', '2017-10-14 11:38:05');
INSERT INTO `coupon` VALUES (3, '年卡', 100, '2017-10-14 11:38:05', '2017-10-14 11:38:05');
INSERT INTO `coupon` VALUES (4, '周卡', 2000, '2017-10-16 13:50:30', '2017-10-16 13:50:30');
INSERT INTO `coupon` VALUES (5, '周卡', 200, '2017-10-16 15:05:36', '2017-10-16 15:05:36');
INSERT INTO `coupon` VALUES (6, 'mkl', 5, '2017-10-16 16:48:30', '2017-10-16 16:48:30');
INSERT INTO `coupon` VALUES (7, '周卡', 10000, '2017-10-19 14:08:21', '2017-10-19 14:08:21');
INSERT INTO `coupon` VALUES (8, '年卡', 5000, '2017-10-19 14:26:08', '2017-10-19 14:26:08');
INSERT INTO `coupon` VALUES (9, '半年卡', 50000, '2017-10-19 14:27:11', '2017-10-19 14:27:11');
INSERT INTO `coupon` VALUES (10, '两年卡', 1000, '2017-10-19 14:28:18', '2017-10-19 14:28:18');
INSERT INTO `coupon` VALUES (11, '月卡', 1000, '2017-10-19 14:31:53', '2017-10-19 14:31:53');
INSERT INTO `coupon` VALUES (12, '三年卡', 500, '2017-10-19 14:35:07', '2017-10-19 14:35:07');
INSERT INTO `coupon` VALUES (13, '360', 100, '2017-10-23 11:55:44', '2017-10-23 11:55:44');
INSERT INTO `coupon` VALUES (14, '0', 0, '2017-10-24 19:31:36', '2017-10-24 19:31:36');
INSERT INTO `coupon` VALUES (15, '11', 11, '2017-10-24 19:46:48', '2017-10-24 19:46:48');
INSERT INTO `coupon` VALUES (16, '12', 11, '2017-10-24 19:48:10', '2017-10-24 19:48:10');
INSERT INTO `coupon` VALUES (17, '50', 1000, '2017-10-24 20:03:12', '2017-10-24 20:03:12');
INSERT INTO `coupon` VALUES (18, '60', 1000, '2017-10-24 20:10:22', '2017-10-24 20:10:22');
INSERT INTO `coupon` VALUES (19, '70', 1000, '2017-10-24 20:11:35', '2017-10-24 20:11:35');

-- ----------------------------
-- Table structure for cust_detail
-- ----------------------------
DROP TABLE IF EXISTS `cust_detail`;
CREATE TABLE `cust_detail`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `cust_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `city` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tel` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int NULL DEFAULT NULL,
  `input_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 611 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust_detail
-- ----------------------------
INSERT INTO `cust_detail` VALUES (463, '20191111000011', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (464, '20191111000012', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (465, '20191111000013', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (466, '20191111000021', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (467, '20191111000014', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (468, '20191111000015', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (469, '20191111000022', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (470, '20191111000001', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (471, '20191111000016', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (472, '20191111000023', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (473, '20191111000002', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (474, '20191111000017', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (475, '20191111000024', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (476, '20191111000003', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (477, '20191111000018', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (478, '20191111000025', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (479, '20191111000004', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (480, '20191111000019', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (481, '20191111000026', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (482, '20191111000005', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (483, '20191111000020', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (484, '20191111000027', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (485, '20191111000006', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (486, '20191111000028', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (487, '20191111000007', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (488, '20191111000029', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (489, '20191111000008', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (490, '20191111000030', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (491, '20191111000009', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (492, '20191111000010', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_detail` VALUES (493, '20191111000031', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (494, '20191111000041', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (495, '20191111000051', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (496, '20191111000032', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (497, '20191111000042', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (498, '20191111000052', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (499, '20191111000033', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (500, '20191111000043', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (501, '20191111000053', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (502, '20191111000034', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (503, '20191111000054', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (504, '20191111000044', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (505, '20191111000035', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (506, '20191111000045', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (507, '20191111000055', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (508, '20191111000036', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (509, '20191111000046', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (510, '20191111000056', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (511, '20191111000037', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (512, '20191111000047', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (513, '20191111000057', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (514, '20191111000038', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (515, '20191111000048', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (516, '20191111000058', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (517, '20191111000039', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (518, '20191111000049', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (519, '20191111000059', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (520, '20191111000040', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (521, '20191111000050', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (522, '20191111000060', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_detail` VALUES (523, '20191111000061', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (524, '20191111000062', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (525, '20191111000063', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (526, '20191111000064', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (527, '20191111000065', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (528, '20191111000071', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (529, '20191111000066', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (530, '20191111000072', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (531, '20191111000073', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (532, '20191111000081', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (533, '20191111000074', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (534, '20191111000067', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (535, '20191111000082', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (536, '20191111000075', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (537, '20191111000068', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (538, '20191111000083', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (539, '20191111000076', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (540, '20191111000069', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (541, '20191111000084', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (542, '20191111000077', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (543, '20191111000070', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (544, '20191111000085', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (545, '20191111000078', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (546, '20191111000086', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (547, '20191111000079', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (548, '20191111000087', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (549, '20191111000080', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (550, '20191111000088', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (551, '20191111000089', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (552, '20191111000090', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:42', '2019-11-23 19:09:42');
INSERT INTO `cust_detail` VALUES (553, '20191111000091', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (554, '20191111000092', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (555, '20191111000093', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (556, '20191111000094', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (557, '20191111000095', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (558, '20191111000096', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (559, '20191111000097', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (560, '20191111000098', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (561, '20191111000099', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (562, '20191111000100', '北京', '小花', '17788998880', 12, '2019-11-23 19:09:52', '2019-11-23 19:09:52');
INSERT INTO `cust_detail` VALUES (563, '202001011', NULL, '小麻花2', '18729829103', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (564, '202001014', NULL, '小麻花5', '18729829106', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (565, '202001017', NULL, '小麻花8', '18729829109', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (566, '2020010110', NULL, '小麻花11', '187298291012', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (567, '2020010113', NULL, '小麻花14', '187298291015', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (568, '2020010116', NULL, '小麻花17', '187298291018', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (569, '2020010119', NULL, '小麻花20', '187298291021', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (570, '2020010122', NULL, '小麻花23', '187298291024', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (571, '2020010125', NULL, '小麻花26', '187298291027', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (572, '2020010128', NULL, '小麻花29', '187298291030', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (573, '2020010131', NULL, '小麻花32', '187298291033', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (574, '2020010134', NULL, '小麻花35', '187298291036', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (575, '20200713000011', NULL, '小麻花11', '1872987291011', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (576, '20200713000012', NULL, '小麻花12', '1872987291012', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (577, '20200713000013', NULL, '小麻花13', '1872987291013', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (578, '20200713000014', NULL, '小麻花14', '1872987291014', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (579, '20200713000015', NULL, '小麻花15', '1872987291015', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (580, '20200713000016', NULL, '小麻花16', '1872987291016', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (581, '20200713000017', NULL, '小麻花17', '1872987291017', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (582, '20200713000018', NULL, '小麻花18', '1872987291018', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (583, '20200713000019', NULL, '小麻花19', '1872987291019', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (584, '20200713000020', NULL, '小麻花20', '1872987291020', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (585, '20200713000021', NULL, '小麻花21', '1872987291021', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (586, '20200713000022', NULL, '小麻花22', '1872987291022', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (587, '20200713000023', NULL, '小麻花23', '1872987291023', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (588, '20200713000024', NULL, '小麻花24', '1872987291024', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (589, '20200713000025', NULL, '小麻花25', '1872987291025', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (590, '20200713000026', NULL, '小麻花26', '1872987291026', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (591, '20200713000027', NULL, '小麻花27', '1872987291027', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (592, '20200713000028', NULL, '小麻花28', '1872987291028', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (593, '20200713000011', NULL, '小麻花11', '1872987291011', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (594, '20200713000012', NULL, '小麻花12', '1872987291012', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (595, '20200713000013', NULL, '小麻花13', '1872987291013', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (596, '20200713000014', NULL, '小麻花14', '1872987291014', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (597, '20200713000015', NULL, '小麻花15', '1872987291015', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (598, '20200713000016', NULL, '小麻花16', '1872987291016', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (599, '20200713000017', NULL, '小麻花17', '1872987291017', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (600, '20200713000018', NULL, '小麻花18', '1872987291018', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (601, '20200713000019', NULL, '小麻花19', '1872987291019', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (602, '20200713000020', NULL, '小麻花20', '1872987291020', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (603, '20200713000021', NULL, '小麻花21', '1872987291021', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (604, '20200713000022', NULL, '小麻花22', '1872987291022', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (605, '20200713000023', NULL, '小麻花23', '1872987291023', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (606, '20200713000024', NULL, '小麻花24', '1872987291024', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (607, '20200713000025', NULL, '小麻花25', '1872987291025', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (608, '20200713000026', NULL, '小麻花26', '1872987291026', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (609, '20200713000027', NULL, '小麻花27', '1872987291027', NULL, NULL, NULL);
INSERT INTO `cust_detail` VALUES (610, '20200713000028', NULL, '小麻花28', '1872987291028', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for cust_info
-- ----------------------------
DROP TABLE IF EXISTS `cust_info`;
CREATE TABLE `cust_info`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `cust_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `input_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust_info
-- ----------------------------
INSERT INTO `cust_info` VALUES (2, '20191111000001', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (3, '20191111000002', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (4, '20191111000003', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (5, '20191111000004', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (6, '20191111000005', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (7, '20191111000006', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (8, '20191111000007', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (9, '20191111000008', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (10, '20191111000009', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (11, '20191111000010', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (12, '20191111000011', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (13, '20191111000012', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (14, '20191111000013', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (15, '20191111000014', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (16, '20191111000015', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (17, '20191111000016', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (18, '20191111000017', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (19, '20191111000018', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (20, '20191111000019', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (21, '20191111000020', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (22, '20191111000021', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (23, '20191111000022', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (24, '20191111000023', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (25, '20191111000024', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (26, '20191111000025', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (27, '20191111000026', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (28, '20191111000027', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (29, '20191111000028', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (30, '20191111000029', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (31, '20191111000030', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (32, '20191111000031', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (33, '20191111000032', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (34, '20191111000033', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (35, '20191111000034', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (36, '20191111000035', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (37, '20191111000036', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (38, '20191111000037', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (39, '20191111000038', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (40, '20191111000039', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (41, '20191111000040', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (42, '20191111000041', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (43, '20191111000042', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (44, '20191111000043', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (45, '20191111000044', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (46, '20191111000045', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (47, '20191111000046', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (48, '20191111000047', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (49, '20191111000048', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (50, '20191111000049', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (51, '20191111000050', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (52, '20191111000051', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (53, '20191111000052', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (54, '20191111000053', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (55, '20191111000054', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (56, '20191111000055', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (57, '20191111000056', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (58, '20191111000057', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (59, '20191111000058', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (60, '20191111000059', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (61, '20191111000060', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (62, '20191111000061', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (63, '20191111000062', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (64, '20191111000063', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (65, '20191111000064', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (66, '20191111000065', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (67, '20191111000066', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (68, '20191111000067', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (69, '20191111000068', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (70, '20191111000069', 1, '处理成功', '2019-11-23 10:45:04', '2019-11-23 10:45:04');
INSERT INTO `cust_info` VALUES (71, '20191111000070', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (72, '20191111000071', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (73, '20191111000072', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (74, '20191111000073', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (75, '20191111000074', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (76, '20191111000075', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (77, '20191111000076', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (78, '20191111000077', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (79, '20191111000078', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (80, '20191111000079', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (81, '20191111000080', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (82, '20191111000081', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (83, '20191111000082', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (84, '20191111000083', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (85, '20191111000084', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (86, '20191111000085', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (87, '20191111000086', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (88, '20191111000087', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (89, '20191111000088', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (90, '20191111000089', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (91, '20191111000090', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (92, '20191111000091', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (93, '20191111000092', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (94, '20191111000093', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (95, '20191111000094', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (96, '20191111000095', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (97, '20191111000096', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (98, '20191111000097', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (99, '20191111000098', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (100, '20191111000099', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');
INSERT INTO `cust_info` VALUES (101, '20191111000100', 1, '处理成功', '2019-11-23 10:45:05', '2019-11-23 10:45:05');

-- ----------------------------
-- Table structure for cust_query_record
-- ----------------------------
DROP TABLE IF EXISTS `cust_query_record`;
CREATE TABLE `cust_query_record`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `record_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `page` int NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `busi_date` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `input_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cust_query_record
-- ----------------------------
INSERT INTO `cust_query_record` VALUES (1, '2019-11-23_1', 1, '30', '2019-11-23', '2019-11-23 19:09:12', '2019-11-23 19:09:12');
INSERT INTO `cust_query_record` VALUES (2, '2019-11-23_2', 2, '30', '2019-11-23', '2019-11-23 19:09:22', '2019-11-23 19:09:22');
INSERT INTO `cust_query_record` VALUES (3, '2019-11-23_3', 3, '30', '2019-11-23', '2019-11-23 19:09:32', '2019-11-23 19:09:32');
INSERT INTO `cust_query_record` VALUES (4, '2019-11-23_4', 4, '10', '2019-11-23', '2019-11-23 19:09:42', '2019-11-23 19:09:42');

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `dept_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门编号',
  `dept_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `dept_location` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门所在地',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modify` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`dept_id`) USING BTREE,
  UNIQUE INDEX `dept_name`(`dept_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES ('rsf', '财务部', '河南省郑州市', '2017-10-14 11:39:48', '2017-10-14 11:39:48');
INSERT INTO `dept` VALUES ('rso', '运营总部', '河南省郑州市', '2017-10-14 11:39:48', '2017-10-14 11:39:48');
INSERT INTO `dept` VALUES ('rsp', '人事部', '河南省郑州市', '2017-10-14 11:39:48', '2017-10-14 11:39:48');

-- ----------------------------
-- Table structure for donate
-- ----------------------------
DROP TABLE IF EXISTS `donate`;
CREATE TABLE `donate`  (
  `donate_id` tinyint NOT NULL AUTO_INCREMENT COMMENT '捐赠id',
  `donate_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '捐赠者姓名',
  `donate_project` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '捐赠项目',
  `donate_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '捐赠时间',
  PRIMARY KEY (`donate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'this is a donate table!!' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of donate
-- ----------------------------
INSERT INTO `donate` VALUES (1, 'liusongshan,体育设备', NULL, '2017-10-25 11:33:04');
INSERT INTO `donate` VALUES (2, 'liusongshanss,爱心午餐', NULL, '2017-10-25 11:34:06');

-- ----------------------------
-- Table structure for emp_token
-- ----------------------------
DROP TABLE IF EXISTS `emp_token`;
CREATE TABLE `emp_token`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '序号',
  `emp_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工号',
  `photo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '头像',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `dept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `token` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '口令',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '员工token表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emp_token
-- ----------------------------
INSERT INTO `emp_token` VALUES (1, 'rso1', 'default', '何鑫', NULL, '3d2c7263-9248-4d27-b796-ea5afa77cdf7');
INSERT INTO `emp_token` VALUES (2, 'rsf2', 'default', '靖品', NULL, '62404db8-30ca-44b0-84b8-5cc77b4cb433');
INSERT INTO `emp_token` VALUES (3, 'rsp3', 'default', '武志诚', NULL, 'bcaecc01-970d-4f0c-aa29-b00312137812');
INSERT INTO `emp_token` VALUES (4, 'rso4', 'default', '黄佳玮', NULL, '9e463f30-138b-4314-85f3-4ca1b127595b');
INSERT INTO `emp_token` VALUES (5, 'rsf3', 'default', '靖品', NULL, '617121db-9604-48fa-8d19-6dba5ba6684b');
INSERT INTO `emp_token` VALUES (6, 'rsp2', 'default', '武志诚', NULL, '07aa26b7-2e33-4e8f-8cfd-2c34c3f640b5');

-- ----------------------------
-- Table structure for express_order
-- ----------------------------
DROP TABLE IF EXISTS `express_order`;
CREATE TABLE `express_order`  (
  `express_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '快递订单编号',
  `site_emp_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工编号',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编号',
  `cargo_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物编号',
  `express_sendname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '寄件人姓名',
  `express_sendphone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '寄件人联系方式',
  `express_receivename` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收件人姓名',
  `express_receiveaddress` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货地址',
  `express_receivephone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收件人联系方式',
  `express_price` decimal(10, 2) UNSIGNED NOT NULL COMMENT '订单价格',
  `express_paytype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单支付方式',
  `express_date` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`express_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '快递订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of express_order
-- ----------------------------
INSERT INTO `express_order` VALUES ('20010001', '333', '1', '2001', '刘松山', '13709564512', '德玛', '德玛西亚', '13709564512', 30.00, '微信', '2017-10-11 16:05:33', '2017-10-17 20:16:44');
INSERT INTO `express_order` VALUES ('20010002', '334', '1', '2002', '刘松山', '13546924124', '赵信', '德玛西亚', '13654984215', 40.00, '支付宝', '2017-10-11 17:13:33', '2017-10-17 20:16:41');
INSERT INTO `express_order` VALUES ('20141001', '124', '1', '5000', '刘', '12164565464', '154', '454', '54645645645', 34.00, '支付宝', '2017-10-19 17:50:44', '2017-10-19 17:50:46');

-- ----------------------------
-- Table structure for manager_emp
-- ----------------------------
DROP TABLE IF EXISTS `manager_emp`;
CREATE TABLE `manager_emp`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '序号',
  `emp_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工号//部门编号加序号',
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `dept_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门编号',
  `emp_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工名称',
  `emp_phone` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工联系方式',
  `emp_idcard` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '身份证号',
  `emp_address` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工住址',
  `emp_sex` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '性别',
  `emp_job` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职位',
  `emp_salary` decimal(8, 2) NOT NULL COMMENT '月薪',
  `emp_remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `photo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'default' COMMENT '头像',
  `emp_hiredate` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '入职日期',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '总部员工表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manager_emp
-- ----------------------------
INSERT INTO `manager_emp` VALUES (1, 'rso1', '1234', 'rso', '何鑫', '13526456544', '455654544123654552', '河南省郑州市', '男', '运营总部经理', 8000.00, '迷之自信', 'default', '2017-10-24 11:34:09', '2017-10-24 11:39:30');
INSERT INTO `manager_emp` VALUES (2, 'rsp2', '1234', 'rsp', '武志成', '15645236545', '452654523654521255', '河南省周口市', '男', '人事部经理', 8000.00, '好好学习', 'default', '2017-10-24 11:38:42', '2017-10-25 12:05:15');
INSERT INTO `manager_emp` VALUES (3, 'rsf3', '1234', 'rsf', '靖品', '15654585654', '455265455265458554', '河南省南阳市', '女', 'cfo', 8000.00, '好好学习', 'default', '2017-10-24 11:41:45', '2017-10-24 11:41:45');
INSERT INTO `manager_emp` VALUES (6, '1', '1', '1', '1', '1', '1', '1', '1', '124', 1.00, '1', '124', '2017-10-24 19:18:40', '2017-10-24 19:18:40');

-- ----------------------------
-- Table structure for pdman_db_version
-- ----------------------------
DROP TABLE IF EXISTS `pdman_db_version`;
CREATE TABLE `pdman_db_version`  (
  `DB_VERSION` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VERSION_DESC` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CREATED_TIME` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pdman_db_version
-- ----------------------------

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int NULL DEFAULT NULL,
  `nation` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of person
-- ----------------------------

-- ----------------------------
-- Table structure for recharge_order
-- ----------------------------
DROP TABLE IF EXISTS `recharge_order`;
CREATE TABLE `recharge_order`  (
  `recharge_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '充值订单编号',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户编号',
  `recharge_money` decimal(10, 0) NULL DEFAULT NULL COMMENT '充值金额',
  `recharge_paytype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '充值方式',
  `recharge_date` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '充值时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`recharge_id`) USING BTREE,
  UNIQUE INDEX `Index 2`(`recharge_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户充值订单明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of recharge_order
-- ----------------------------
INSERT INTO `recharge_order` VALUES ('11', '11', 11, '11', '2017-10-13 13:13:27', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('20171013131445', '11', 100, NULL, NULL, '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('20171013131617', '100', 100, NULL, '2017-10-13 13:16:15', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('20171013131732', '100', 100, '支付宝', '2017-10-13 13:17:29', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('2017101314128', '154', 1000, '微信', '2017-10-13 14:01:25', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('201710131498', '11', 11111, '支付宝', '2017-10-13 14:09:05', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('2017101693412', '12', 1000, '支付宝', '2017-10-16 09:34:14', '2017-10-16 09:34:14');
INSERT INTO `recharge_order` VALUES ('20171017104036', '156', 1000, '支付宝', '2017-10-17 10:40:40', '2017-10-17 10:40:40');
INSERT INTO `recharge_order` VALUES ('2017101710414', '123', 1656, '微信', '2017-10-17 10:41:07', '2017-10-17 10:41:07');
INSERT INTO `recharge_order` VALUES ('20171019141616', '111', 5000, '支付宝', '2017-10-19 14:16:23', '2017-10-19 14:16:23');

-- ----------------------------
-- Table structure for rollover_order
-- ----------------------------
DROP TABLE IF EXISTS `rollover_order`;
CREATE TABLE `rollover_order`  (
  `rollover_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '转仓订单编号',
  `cargo_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '货物编号',
  `paytype` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付方式',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户编号',
  `rollover_dest_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标站点编号',
  `rollover_start_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发出站点编号',
  `rollover_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '订单金额',
  `rollover_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`rollover_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '转仓订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rollover_order
-- ----------------------------
INSERT INTO `rollover_order` VALUES ('8001', '2001', '微信', '1', '1', '003', 215.00, '2017-10-24 10:30:30', '2017-10-24 10:30:31');
INSERT INTO `rollover_order` VALUES ('8002', '2002', '微信', '2', '3', '004', 999.00, '2017-10-24 10:31:04', '2017-10-24 10:31:40');
INSERT INTO `rollover_order` VALUES ('8003', '2003', '支付宝', '3', '4', '005', 1010.00, '2017-10-24 10:31:37', '2017-10-24 10:31:48');
INSERT INTO `rollover_order` VALUES ('8004', '2004', '支付宝', '3', '5', '006', 997.00, '2017-10-24 10:32:12', '2017-10-24 10:33:15');
INSERT INTO `rollover_order` VALUES ('8005', '2005', '微信', '4', '6', '007', 1013.00, '2017-10-24 10:32:20', '2017-10-24 10:33:36');
INSERT INTO `rollover_order` VALUES ('8006', '2006', '支付宝', '5', '7', '008', 1037.00, '2017-10-24 10:32:27', '2017-10-24 10:33:40');

-- ----------------------------
-- Table structure for site
-- ----------------------------
DROP TABLE IF EXISTS `site`;
CREATE TABLE `site`  (
  `site_id` int NOT NULL COMMENT '站点id',
  `dept_id` int NULL DEFAULT 12 COMMENT '部门id',
  `site_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '站点名字',
  `site_proniace` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属省份',
  `site_city` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属城市',
  `site_linkman` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `site_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系号码',
  `gmt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`site_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '站点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of site
-- ----------------------------
INSERT INTO `site` VALUES (3, 12, '二七站', '河南省', '郑州市', '比克', '15874859632', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES (4, 12, '高新站', '河南省', '郑州市', '克林', '15485423564', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES (5, 12, '高新站', '河南省', '郑州市', '比鲁斯', '14747474747', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES (6, 12, '二七站', '河南省', '郑州市', '18号', '12365478966', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES (7, 12, '灌江口', '西牛贺州', '丽江', '哮天犬', '12369854744', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES (8, 12, '九嶷山', '中土神州', '苍梧之渊', '民', '12365895474', '2017-10-14 08:49:22', '2017-10-17 12:11:11');
INSERT INTO `site` VALUES (9, 12, '天毒', '东胜神州', '隅有国', '小明', '16587458965', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES (10, 12, '三川', '北俱芦洲', '雒阳', '八戒·', '15698745698', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES (21, 12, '二七', '河南', '郑州', '航航那个', '15874589658', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES (23, 10, '高新站', '河南省', '郑州市', '小诚诚', '13719596418', '2017-10-14 08:49:22', '2017-10-14 09:55:31');
INSERT INTO `site` VALUES (24, 12, '郑州大学', '河南', '郑州', '王大虎', '12312312345', '2017-10-20 16:19:24', '2017-10-20 16:19:24');
INSERT INTO `site` VALUES (25, 12, '梧桐街', '河南', '郑州', '王大虎', '34534534567', '2017-10-20 16:27:46', '2017-10-20 16:27:46');
INSERT INTO `site` VALUES (26, 12, '河南工业大学', '河南', '郑州', '王大虎', '12345678909', '2017-10-20 16:35:35', '2017-10-20 16:35:35');
INSERT INTO `site` VALUES (12354, 12, '地方', '是打发点', '电风扇广东省', '第三方割发代首', '12521252454', '2017-10-24 17:11:54', '2017-10-24 17:12:13');
INSERT INTO `site` VALUES (123542, 12, '的方式公开', '第三方', '都是个', '十多个', '15454545454', '2017-10-24 17:15:05', '2017-10-24 17:15:05');

-- ----------------------------
-- Table structure for site_emp
-- ----------------------------
DROP TABLE IF EXISTS `site_emp`;
CREATE TABLE `site_emp`  (
  `site_emp_id` int UNSIGNED NOT NULL COMMENT '站点员工编号',
  `site_id` int UNSIGNED NOT NULL COMMENT '站点编号',
  `site_emp_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工姓名',
  `site_emp_phone` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工联系方法',
  `site_emp_idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '身份证号',
  `site_emp_address` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系地址',
  `site_emp_sex` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工性别',
  `site_emp_salary` decimal(8, 2) UNSIGNED NOT NULL COMMENT '薪资',
  `site_emp_job` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职位',
  `site_emp_remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `mgt_create` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_mondified` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`site_emp_id`) USING BTREE,
  UNIQUE INDEX `site_emp_idcard`(`site_emp_idcard`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '站点员工表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of site_emp
-- ----------------------------
INSERT INTO `site_emp` VALUES (10, 10, '武志成', '158965235', '10456556', '按时发打发', '男', 10551.20, '丰富', '安慰', '2017-10-15 00:00:00', '2017-10-19 00:00:00');
INSERT INTO `site_emp` VALUES (13, 2, '1', '1', '1', '1', '1', 1.00, '1', '1', '2017-10-24 19:21:14', '2017-10-24 19:21:14');
INSERT INTO `site_emp` VALUES (102, 1, '白浅', '1587544747', '156687455896321456', '包子山', '男', 12215.00, '卖烧饼', '卖烧饼喽', '2017-10-20 00:00:00', '2017-10-20 00:00:00');
INSERT INTO `site_emp` VALUES (103, 1, '夜华', '56987458965', '125888955547856963', '景阳冈', '女', 12365.00, '打虎', '打虎很厉害', '2017-10-20 00:00:00', '2017-10-20 00:00:00');
INSERT INTO `site_emp` VALUES (106, 1, '刘航', '15487458965', '410225555569999987', '包子山', '男', 125.00, '卖烧饼', '卖烧饼喽', '2017-10-20 00:00:00', '2017-10-20 00:00:00');
INSERT INTO `site_emp` VALUES (342, 342, '你忙吧', '4324', '242', '423', '42', 24234.00, '234', '234', '2017-10-23 13:07:07', '2017-10-23 13:07:07');
INSERT INTO `site_emp` VALUES (684, 6, '刘航', '86552', '1654656151', '恶化覅u', '男', 4555.23, '集合', '收款金额非', '2017-10-17 00:00:00', '2017-10-17 00:00:00');
INSERT INTO `site_emp` VALUES (5566, 3, '冯菁文', '18703615087', '412727198905062356', '是自己的方便交流', '女', 15956.23, '搬运工', '我是一个吃货', '2017-10-16 00:00:00', '2017-10-16 00:00:00');
INSERT INTO `site_emp` VALUES (5656, 1, '靖品', '15698652314', '412756895655', '南阳市锦州', '女', 3215.23, '卖烧饼', '卖烧饼喽', '2017-10-17 00:00:00', '2017-10-20 00:00:00');
INSERT INTO `site_emp` VALUES (16264, 9826, '圣斗士', '68416', '55565236', '富国海底世界', '女', 86165.00, '打工人工', '导入', '2017-10-19 00:00:00', '2017-10-19 00:00:00');
INSERT INTO `site_emp` VALUES (5516516, 5115, '爱妃', '565155', '51598555', '啊色粉sa', '女', 14.00, '561', '51', '2017-10-21 14:53:01', '2017-10-21 14:53:01');

-- ----------------------------
-- Table structure for store_order
-- ----------------------------
DROP TABLE IF EXISTS `store_order`;
CREATE TABLE `store_order`  (
  `store_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '存储订单编号',
  `site_emp_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务员编号',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编号',
  `user_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '交易人姓名',
  `cargo_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物编号',
  `contacts_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系人姓名',
  `contacts_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `contacts_addr` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系地址',
  `amount` decimal(5, 2) NOT NULL COMMENT '订单金额',
  `paytype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支付方式',
  `store_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `cargo_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '物品类型',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `chose_area` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '选择地区'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '存储订单表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of store_order
-- ----------------------------
INSERT INTO `store_order` VALUES ('9001', '5656', '1001', '夜华', '2001', '白浅', '15138685092', '青邱', 999.99, '微信', '2017-10-24 10:52:17', '2017-10-24 10:52:19', '桃花醉', '易碎品', '郑州');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
  `parent_id` int NULL DEFAULT NULL COMMENT '父菜单ID',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `icon` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `type` int NULL DEFAULT NULL COMMENT '类型',
  `permission` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menu_rank` int NULL DEFAULT NULL COMMENT '菜单级别',
  `status` int NULL DEFAULT NULL COMMENT '状态',
  `creadted_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `creadted_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源表 ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (1, 0, '系统首页', 'dashboard', 'el-icon-lx-home', 0, NULL, 1, 1, 'admin', '2019-07-05 23:24:21', 'admin', '2019-07-05 23:24:27');
INSERT INTO `sys_permission` VALUES (2, 0, '基础表格', 'table', 'el-icon-lx-cascades', 0, NULL, 1, 1, 'admin', '2019-07-05 23:25:35', 'admin', '2019-07-05 23:25:38');
INSERT INTO `sys_permission` VALUES (3, 0, '用户管理', 'user', 'el-icon-lx-people', 0, NULL, 1, 1, 'admin', '2019-07-05 23:26:48', 'admin', '2019-07-05 23:26:51');
INSERT INTO `sys_permission` VALUES (4, 0, 'tab选项卡', 'tabs', 'el-icon-lx-copy', 0, NULL, 1, 1, 'admin', '2019-07-05 23:29:02', 'admin', '2019-07-05 23:29:05');
INSERT INTO `sys_permission` VALUES (5, 0, '表单相关', '3', 'el-icon-lx-calendar', 1, NULL, 1, 1, 'admin', '2019-07-05 23:30:56', 'admin', '2019-07-05 23:31:07');
INSERT INTO `sys_permission` VALUES (6, 5, '基本表单', 'form', NULL, 0, NULL, 2, 1, 'admin', '2019-07-05 23:31:56', 'admin', '2019-07-05 23:32:03');
INSERT INTO `sys_permission` VALUES (7, 5, '三级菜单', '3-2', NULL, 1, NULL, 2, 1, 'admin', '2019-07-05 23:34:04', 'admin', '2019-07-05 23:34:08');
INSERT INTO `sys_permission` VALUES (8, 7, '富文本编辑器', 'editor', NULL, 0, NULL, 3, 1, 'admin', '2019-07-05 23:34:53', 'admin', '2019-07-05 23:34:57');
INSERT INTO `sys_permission` VALUES (9, 7, 'markdown编辑器', 'markdown', NULL, 0, NULL, 3, 1, 'admin', '2019-07-05 23:36:02', 'admin', '2019-07-05 23:36:05');
INSERT INTO `sys_permission` VALUES (10, 5, '文件上传', 'upload', NULL, 0, NULL, 2, 1, 'admin', '2019-07-05 23:37:03', 'admin', '2019-07-05 23:37:06');
INSERT INTO `sys_permission` VALUES (11, 0, '自定义图标', 'icon', 'el-icon-lx-emoji', 0, NULL, 1, 1, 'admin', '2019-07-05 23:38:19', 'admin', '2019-07-05 23:38:23');
INSERT INTO `sys_permission` VALUES (12, 0, 'schart图表', 'charts', 'el-icon-pie-chart', 0, NULL, 1, 1, 'admin', '2019-07-05 23:39:04', 'admin', '2019-07-05 23:39:07');
INSERT INTO `sys_permission` VALUES (13, 0, '拖拽组件', '6', 'el-icon-rank', 1, NULL, 1, 1, 'admin', '2019-07-05 23:41:18', 'admin', '2019-07-05 23:41:22');
INSERT INTO `sys_permission` VALUES (14, 13, '拖拽列表', 'drag', NULL, 0, NULL, 2, 1, 'admin', '2019-07-05 23:42:13', 'admin', '2019-07-05 23:42:16');
INSERT INTO `sys_permission` VALUES (15, 13, '拖拽弹框', 'dialog', NULL, 0, NULL, 2, 1, 'admin', '2019-07-05 23:42:54', 'admin', '2019-07-05 23:42:57');
INSERT INTO `sys_permission` VALUES (16, 0, '国际化功能', 'i18n', 'el-icon-lx-global', 0, NULL, 1, 1, 'admin', '2019-07-05 23:43:47', 'admin', '2019-07-05 23:43:50');
INSERT INTO `sys_permission` VALUES (17, 0, '错误处理', '7', 'el-icon-lx-warn', 1, NULL, 1, 1, 'admin', '2019-07-05 23:44:33', 'admin', '2019-07-05 23:44:36');
INSERT INTO `sys_permission` VALUES (18, 17, '权限测试', 'permission', NULL, 0, NULL, 2, 1, 'admin', '2019-07-05 23:45:47', 'admin', '2019-07-05 23:45:50');
INSERT INTO `sys_permission` VALUES (19, 17, '404页面', '404', NULL, 0, NULL, 2, 1, 'admin', '2019-07-05 23:46:34', 'admin', '2019-07-05 23:46:37');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编号',
  `role_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `status` int NOT NULL DEFAULT 0 COMMENT '角色状态',
  `description` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `creadted_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `creadted_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统角色表 ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'admin', '系统管理员', 0, NULL, 'admin', '2019-07-05 23:47:47', 'admin', '2019-07-05 23:47:50');
INSERT INTO `sys_role` VALUES (2, 'developer', '研发岗', 0, NULL, 'admin', '2019-07-05 23:49:59', 'admin', '2019-07-05 23:50:02');
INSERT INTO `sys_role` VALUES (3, 'sys0001', '用户管理员', 0, NULL, 'admin', '2019-07-05 23:50:40', 'admin', '2019-07-05 23:50:43');
INSERT INTO `sys_role` VALUES (4, 'sys0002', '角色管理员', 0, NULL, 'admin', '2019-07-05 23:51:16', 'admin', '2019-07-05 23:51:20');
INSERT INTO `sys_role` VALUES (5, 'sys0003', '资源管理员', 0, NULL, 'admin', '2019-07-05 23:52:03', 'admin', '2019-07-05 23:52:05');
INSERT INTO `sys_role` VALUES (6, 'sys0004', '用户角色关系管理', 0, NULL, 'admin', '2019-07-05 23:53:12', 'admin', '2019-07-05 23:53:15');
INSERT INTO `sys_role` VALUES (7, 'sys0005', '角色资源关系管理', 0, NULL, 'admin', '2019-07-05 23:54:03', 'admin', '2019-07-05 23:54:06');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色编号',
  `permission_id` int NULL DEFAULT NULL COMMENT '资源编号',
  `creadted_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `creadted_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与资源对应关系 ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (1, 'admin', 1, 'admin', '2019-07-07 18:01:48', 'admin', '2019-07-07 18:02:03');
INSERT INTO `sys_role_permission` VALUES (2, 'admin', 2, 'admin', '2019-07-07 18:02:43', 'admin', '2019-07-07 18:02:52');
INSERT INTO `sys_role_permission` VALUES (3, 'admin', 3, 'admin', '2019-07-07 18:03:07', 'admin', '2019-07-07 18:03:12');
INSERT INTO `sys_role_permission` VALUES (4, 'admin', 4, 'admin', '2019-07-07 18:03:27', 'admin', '2019-07-07 18:03:33');
INSERT INTO `sys_role_permission` VALUES (5, 'admin', 5, 'admin', '2019-07-07 18:04:19', 'admin', '2019-07-07 18:04:22');
INSERT INTO `sys_role_permission` VALUES (6, 'admin', 6, 'admin', '2019-07-07 18:04:48', 'admin', '2019-07-07 18:04:52');
INSERT INTO `sys_role_permission` VALUES (7, 'admin', 7, 'admin', '2019-07-07 18:05:21', 'admin', '2019-07-07 18:05:26');
INSERT INTO `sys_role_permission` VALUES (8, 'admin', 8, 'admin', '2019-07-07 18:06:15', 'admin', '2019-07-07 18:06:21');
INSERT INTO `sys_role_permission` VALUES (9, 'admin', 9, 'admin', '2019-07-07 18:07:00', 'admin', '2019-07-07 18:07:03');
INSERT INTO `sys_role_permission` VALUES (10, 'admin', 10, 'admin', '2019-07-07 18:07:16', 'admin', '2019-07-07 18:07:21');
INSERT INTO `sys_role_permission` VALUES (11, 'admin', 11, 'admin', '2019-07-07 18:07:34', 'admin', '2019-07-07 18:07:38');
INSERT INTO `sys_role_permission` VALUES (12, 'admin', 12, 'admin', '2019-07-07 18:07:53', 'admin', '2019-07-07 18:07:58');
INSERT INTO `sys_role_permission` VALUES (13, 'admin', 13, 'admin', '2019-07-07 18:08:16', 'admin', '2019-07-07 18:08:21');
INSERT INTO `sys_role_permission` VALUES (14, 'admin', 14, 'admin', '2019-07-07 18:09:28', 'admin', '2019-07-07 18:09:31');
INSERT INTO `sys_role_permission` VALUES (15, 'admin', 15, 'admin', '2019-07-07 18:10:14', 'admin', '2019-07-07 18:10:17');
INSERT INTO `sys_role_permission` VALUES (16, 'admin', 16, 'admin', '2019-07-07 18:10:35', 'admin', '2019-07-07 18:10:40');
INSERT INTO `sys_role_permission` VALUES (17, 'admin', 17, 'admin', '2019-07-07 18:10:54', 'admin', '2019-07-07 18:10:58');
INSERT INTO `sys_role_permission` VALUES (18, 'admin', 18, 'admin', '2019-07-07 18:11:10', 'admin', '2019-07-07 18:11:15');
INSERT INTO `sys_role_permission` VALUES (19, 'admin', 19, 'admin', '2019-07-07 18:11:45', 'admin', '2019-07-07 18:11:49');
INSERT INTO `sys_role_permission` VALUES (20, 'admin', 0, 'admin', '2019-12-28 18:15:44', 'admin', '2019-12-28 18:15:51');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编号',
  `user_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名字',
  `user_phone` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机',
  `user_email` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` tinyint NOT NULL DEFAULT 0 COMMENT '用户状态 0正常，1锁定，2注销',
  `user_photo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'default' COMMENT '用户头像',
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `salt` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_point` int UNSIGNED NULL DEFAULT NULL COMMENT '积分',
  `created_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `updated_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `created_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_nickname`(`user_id`) USING BTREE,
  UNIQUE INDEX `user_phone`(`user_phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'honker、Boy', '刘松山', '15890197161', '937661655@qq.com', 0, 'default', '1314', NULL, 1000, '2017-10-14 09:13:04', '2019-06-15 10:05:03', NULL, NULL);
INSERT INTO `sys_user` VALUES (4, 'xiaoxin', '小新', '18737832326', NULL, 0, 'default', '123456', NULL, 2000, '2017-10-23 12:10:30', '2019-06-27 23:04:36', NULL, NULL);
INSERT INTO `sys_user` VALUES (5, 'fengjinwen', '冯静文', '18703615027', NULL, 0, 'default', '123456', NULL, 2000, '2017-10-23 12:13:57', '2019-06-27 23:04:44', NULL, NULL);
INSERT INTO `sys_user` VALUES (6, 'wuzhicheng', '武志诚', '15138938657', NULL, 0, 'default', '1234', NULL, 2000, '2017-10-23 19:37:35', '2019-06-27 23:04:51', NULL, NULL);
INSERT INTO `sys_user` VALUES (7, 'xiaopang', '苏小胖', '13140001897', NULL, 0, 'default', '1234', NULL, 2000, '2017-10-25 11:59:13', '2019-06-27 23:04:57', NULL, NULL);
INSERT INTO `sys_user` VALUES (8, 'machenglong', '马成龙', '18282928299', NULL, 0, 'default', '1234', NULL, 2000, '2019-06-14 22:47:42', '2019-06-27 23:05:04', NULL, NULL);
INSERT INTO `sys_user` VALUES (9, 'dema', '德玛', '17218728122', NULL, 2, 'default', '1234', NULL, 1000, '2019-06-14 22:48:28', '2019-06-15 11:36:37', NULL, NULL);
INSERT INTO `sys_user` VALUES (10, 'zhaoxin', '赵信', '18728282111', NULL, 0, 'default', '1234', NULL, 2000, '2019-06-14 22:49:02', '2019-06-27 23:05:11', NULL, NULL);
INSERT INTO `sys_user` VALUES (11, 'guanghui', '光辉', '17267281999', NULL, 1, 'default', '1234', NULL, 2000, '2019-06-14 22:49:46', '2019-06-27 23:05:17', NULL, NULL);
INSERT INTO `sys_user` VALUES (12, 'shuguang', '曙光', '17282728211', NULL, 0, 'default', '1234', NULL, 1000, '2019-06-14 22:58:55', '2019-06-26 22:03:08', NULL, NULL);
INSERT INTO `sys_user` VALUES (13, 'aixi', '艾希', '18282821222', NULL, 1, 'default', '1234', NULL, 1000, '2019-06-14 22:59:36', '2019-06-26 23:10:26', NULL, NULL);
INSERT INTO `sys_user` VALUES (14, 'suolaka', '索拉卡', '17282911022', '17282911022@163.com', 0, 'default', '1234', NULL, 2000, '2019-06-26 23:55:55', '2019-06-27 23:04:26', NULL, NULL);
INSERT INTO `sys_user` VALUES (15, 'huangzi', '皇子', '18272829111', '18272829111@163.com', 0, 'default', '1234', NULL, 2000, '2019-06-26 23:57:47', '2019-06-26 23:58:13', NULL, NULL);
INSERT INTO `sys_user` VALUES (16, 'manwang', '蛮王', '18278292011', '18278292011@163.com', 0, 'default', '1234', NULL, 2000, '2019-06-26 23:59:13', '2019-06-27 23:04:18', NULL, NULL);
INSERT INTO `sys_user` VALUES (17, 'gexiaolun', '银河之力', '18728291022', '18728291022@163.com', 0, 'default', '1234', NULL, 2000, '2019-06-27 22:56:45', '2019-06-27 23:04:04', NULL, NULL);
INSERT INTO `sys_user` VALUES (18, 'wei', '蔚', '18272833322', '18272833322@163.com', 0, 'default', '1234', NULL, NULL, '2019-07-03 20:57:35', '2019-07-03 20:57:35', NULL, NULL);
INSERT INTO `sys_user` VALUES (19, 'admin', '系统管理员', '18273829188', '18273829188@163.com', 0, 'default', '1234', NULL, NULL, '2019-07-05 22:59:56', '2019-07-05 22:59:56', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编号',
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色编号',
  `status` int NULL DEFAULT NULL COMMENT '状态',
  `creadted_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `creadted_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色关联表 ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 'admin', 'admin', 1, 'admin', '2019-07-07 17:35:17', 'admin', '2019-07-07 17:35:20');

-- ----------------------------
-- Table structure for user_token
-- ----------------------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '序号',
  `user_id` int UNSIGNED NOT NULL COMMENT '用户编号',
  `photo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '头像',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
  `token` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '口令',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户token表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of user_token
-- ----------------------------
INSERT INTO `user_token` VALUES (5, 1, 'default', 'honker丶boy', '79f03743-1309-4005-b900-3a56ddfcb26f');
INSERT INTO `user_token` VALUES (6, 4, 'default', '小新aaaa', '53164ad1-c814-486e-ad05-e327c8bf34bc');
INSERT INTO `user_token` VALUES (7, 5, 'default', '冯静文aaa', 'b823d17f-3998-4491-9978-cfd859f38f7a');
INSERT INTO `user_token` VALUES (8, 6, 'default', '武志诚', 'b3e39388-a706-4b06-ad08-5f4c74870263');
INSERT INTO `user_token` VALUES (9, 7, 'default', 'xiaopang', '7fc842ab-cd0e-43ea-8e66-ac10b74d6cae');

SET FOREIGN_KEY_CHECKS = 1;
