import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios';
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
import { messages } from './components/common/i18n';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
// import '../static/css/theme-green/index.css';       // 浅绿色主题
import './assets/css/icon.css';
import './components/common/directives';
import "babel-polyfill";

Vue.config.productionTip = false
Vue.use(VueI18n);
Vue.use(ElementUI, {
    size: 'small'
});
Vue.prototype.$axios = axios;

const i18n = new VueI18n({
    locale: 'zh',
    messages
})

axios.interceptors.response.use(res => {
    if(!res.data.success && res.data.code == 1000001){
        router.replace({
            path: 'login'
        })
        return Promise.reject(res);
    } else {
        // 对响应数据做些什么
        return res
    }
}, err => {
    // 对响应错误做些什么
    console.log('err', err.response) // 修改后
    return Promise.resolve(errsresponse) // 可在组件内获取到服务器返回信息
})

//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    debugger
    const userId = localStorage.getItem('ms_userId');
    if (!userId && to.path !== '/login') {
        next('/login');
    } else if(to.path==="/login"){
        next();
    }else if (to.meta.permission) {
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        userId === 'admin' ? next() : next('/403');
    } else {
        let userMenu = localStorage.getItem('user_menu');
        if(!userMenu){
            var params ={
                userId : userId
            }
            axios.post('/sys/selectUserPermission', params ).then((res) => {
                console.log(res.data)
                if(res.data.success){
                    console.log(res.data.data)
                    userMenu = res.data.data;
                    localStorage.setItem('user_menu',res.data.data);
                    // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
                    if (navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor') {
                        Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
                            confirmButtonText: '确定'
                        });
                    } else {
                        if(to.path !== '/403' && to.path !== '/login'&& userMenu){
                            let result = userMenu.indexOf(to.path);
                            if(result>=0){
                                next();
                            } else{
                                next('/403'); 
                            }
                        } else {
                            next();
                        }
                    }
                } else {
                    Vue.prototype.$alert(res.data.msg, '错误提示', {
                        confirmButtonText: '确定'
                    });
                }
            })
        } else {
            // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
            if (navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor') {
                Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
                    confirmButtonText: '确定'
                });
            } else {
                if(to.path !== '/403' && to.path !== '/login'&& userMenu){
                    let result = userMenu.indexOf(to.path);
                    if(result>=0){
                        next();
                    } else{
                        next('/403'); 
                    }
                } else {
                    next();
                }
            }
        }
    }   
})


new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app')