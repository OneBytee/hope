# hope

#### 介绍
        这个项目想写一些功能性的东西，把平时用的一些技术整合进来。用来熟悉和加深知识理解。
    前端采用lin-xin大神的开源前端模板（https://github.com/lin-xin/vue-manage-system）。

#### 软件架构
    后台:
        jdk: 1.8.0_45
        lombok
        SpringBoot: 2.1.3.RELEASE
        mybatis: 1.3.2
        pagehelper: 1.2.3
        swagger: 2.5.0
        druid: 1.1.10
    前端:
        vue: 2.6.10
        element-ui: 2.8.2
#### 项目进度
框架|版本|日期
---|:---:|---:
shiro|1.4.0|2019-07-03