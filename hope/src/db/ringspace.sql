/*
Navicat MySQL Data Transfer

Source Server         : 本地Mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : ring

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2019-04-05 15:48:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `address_id` varchar(50) NOT NULL COMMENT '地址编号',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `address_person` varchar(20) NOT NULL COMMENT '联系人',
  `address_number` varchar(20) NOT NULL COMMENT '联系方式',
  `address_province` varchar(50) NOT NULL COMMENT '省份',
  `address_city` varchar(50) NOT NULL COMMENT '城市',
  `address_area` varchar(50) NOT NULL COMMENT '区县',
  `address_detaile` varchar(50) NOT NULL COMMENT '信息地址',
  `address_check` int(11) NOT NULL COMMENT '是否设为默认地址 1 默认0不默认',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户地址管理表';

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('HJW12341508299017175', 'HJW1234', '王大虎', '12312312345', '山西省', '太原市', '小店区', '梧桐街126号', '0', '2017-10-18 11:58:56', '2017-10-18 19:53:45');
INSERT INTO `address` VALUES ('HJW12341508327506045', 'HJW1234', '王大虎', '23434567890', '内蒙古自治区', '呼和浩特市', '新城区', '莲花街123号', '0', '2017-10-18 19:53:45', '2017-10-18 19:54:44');
INSERT INTO `address` VALUES ('HJW12341508327565135', 'HJW1234', '王大虎', '23423423415', '北京市', '北京市', '朝阳区', '雪松路123号', '0', '2017-10-18 19:54:44', '2017-10-18 19:55:16');
INSERT INTO `address` VALUES ('HJW12341508327597769', 'HJW1234', '王大虎', '23423456765', '辽宁省', '沈阳市', '和平区', '腊梅路123号', '0', '2017-10-18 19:55:16', '2017-10-18 19:56:54');
INSERT INTO `address` VALUES ('HJW12341508327695478', 'HJW1234', '王大虎', '12312345645', '黑龙江省', '哈尔滨市', '道里区', '科学大道123号', '0', '2017-10-18 19:56:54', '2017-10-18 19:57:21');
INSERT INTO `address` VALUES ('HJW12341508327722113', 'HJW1234', '王大虎', '12345567890', '安徽省', '合肥市', '瑶海区', '翠竹街123号', '0', '2017-10-18 19:57:21', '2017-10-18 20:00:13');
INSERT INTO `address` VALUES ('HJW12341508327895005', 'HJW1234', '王大虎', '12345665432', '河北省', '石家庄市', '长安区', '航海路123号', '0', '2017-10-18 20:00:14', '2017-10-18 20:01:41');
INSERT INTO `address` VALUES ('HJW12341508327982134', 'HJW1234', '王大虎', '12312312345', '吉林省', '长春市', '南关区', '农业路123号', '0', '2017-10-18 20:01:41', '2017-10-19 09:38:13');
INSERT INTO `address` VALUES ('HJW12341508382763345', 'HJW1234', '王大虎', '12312312345', '上海市', '上海市', '黄浦区', '春藤路120号', '0', '2017-10-19 11:14:43', '2017-10-19 11:15:05');
INSERT INTO `address` VALUES ('HJW12341508385674606', 'HJW1234', '王大虎', '12312312345', '浙江省', '杭州市', '上城区', '金水路134号', '0', '2017-10-19 12:03:14', '2017-10-19 14:14:13');
INSERT INTO `address` VALUES ('HJW12341508392177689', 'HJW1234', '王大虎', '123123345577', '青海省', '西宁市', '城东区', '复兴路196号', '0', '2017-10-19 13:51:37', '2017-10-19 13:56:06');
INSERT INTO `address` VALUES ('HJW12341508392446940', 'HJW1234', '王大虎', '12312312345', '河南省', '郑州市', '中原区', '云和数据3号楼', '1', '2017-10-19 13:56:06', '2017-10-19 14:47:27');

-- ----------------------------
-- Table structure for appointment_order
-- ----------------------------
DROP TABLE IF EXISTS `appointment_order`;
CREATE TABLE `appointment_order` (
  `appointment_id` varchar(50) NOT NULL COMMENT '预约订单编号',
  `site_emp_id` varchar(50) DEFAULT NULL COMMENT '业务员编号',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户编号',
  `cargo_type_name` varchar(50) DEFAULT NULL COMMENT '货物类型编号',
  `appointment_name` varchar(50) DEFAULT NULL COMMENT '预约人姓名',
  `appointment_tel` varchar(12) DEFAULT NULL COMMENT '预约人联系方式',
  `appointment_address` varchar(50) DEFAULT NULL COMMENT '预约详细地址',
  `appointment_province` varchar(50) DEFAULT NULL COMMENT '预约地址省份',
  `appointment_city` varchar(50) DEFAULT NULL COMMENT '预约地址市',
  `appointment_area` varchar(50) DEFAULT NULL COMMENT '预约地址区域',
  `appointment_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `status` varchar(50) DEFAULT '0' COMMENT '状态',
  `appointment_pare_time` datetime DEFAULT NULL COMMENT '预约时间',
  PRIMARY KEY (`appointment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户预约订单表';

-- ----------------------------
-- Records of appointment_order
-- ----------------------------
INSERT INTO `appointment_order` VALUES ('1508813125296', null, null, '家庭用品', '刘松山', '15890197161', '电子商务产业园', '河南', '郑州市', '中原区', '2017-10-24 10:46:14', '2017-10-24 10:46:14', '来一份炒面！！', null, '2017-10-24 10:45:10');
INSERT INTO `appointment_order` VALUES ('1508836167576', null, '67576', '2', '武志成', '11011011011', '武大村', '河南省', '周口市', '淮阳县', '2017-10-24 17:10:16', '2017-10-24 17:10:16', '东南角', '待预约', '2017-10-24 17:09:04');
INSERT INTO `appointment_order` VALUES ('1508836444277', null, '44277', '3', 'login', '12012012022', '大王村', '山西省', '长治市', '梧桐县', '2017-10-24 17:14:53', '2017-10-24 17:14:53', '哥哥', '待预约', '2017-10-24 17:13:55');

-- ----------------------------
-- Table structure for cargo
-- ----------------------------
DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo` (
  `cargo_id` varchar(50) NOT NULL COMMENT '货物编号',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `cargo_type_id` int(10) unsigned NOT NULL COMMENT '货物类型编号',
  `cargo_name` varchar(30) NOT NULL COMMENT '货物名称',
  `cargo_weight` decimal(5,2) unsigned NOT NULL COMMENT '货物重量',
  `cargo_value` decimal(7,2) unsigned NOT NULL COMMENT '货物价值',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`cargo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='物品表';

-- ----------------------------
-- Records of cargo
-- ----------------------------

-- ----------------------------
-- Table structure for cargo_path
-- ----------------------------
DROP TABLE IF EXISTS `cargo_path`;
CREATE TABLE `cargo_path` (
  `cargo_path_id` varchar(50) NOT NULL COMMENT '货物路径编号',
  `cargo_id` varchar(50) DEFAULT NULL COMMENT '货物编号',
  `cargo_path_name` varchar(10) DEFAULT NULL COMMENT '路径站名',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`cargo_path_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='货物路径表';

-- ----------------------------
-- Records of cargo_path
-- ----------------------------

-- ----------------------------
-- Table structure for cargo_type
-- ----------------------------
DROP TABLE IF EXISTS `cargo_type`;
CREATE TABLE `cargo_type` (
  `cargo_type_id` int(11) NOT NULL COMMENT '货物类型编号',
  `cargo_type_name` varchar(10) NOT NULL COMMENT '货物类型名称',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`cargo_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='货物分类表';

-- ----------------------------
-- Records of cargo_type
-- ----------------------------
INSERT INTO `cargo_type` VALUES ('1', '家用电器', '2017-10-14 11:36:27', '2017-10-14 11:36:27');
INSERT INTO `cargo_type` VALUES ('2', '家庭用品', '2017-10-14 11:36:27', '2017-10-14 11:36:27');
INSERT INTO `cargo_type` VALUES ('3', '数码产品', '2017-10-14 11:36:27', '2017-10-14 11:36:27');
INSERT INTO `cargo_type` VALUES ('4', '绝密文件', '2017-10-14 11:36:27', '2017-10-14 11:36:27');

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `city_id` varchar(50) NOT NULL COMMENT '城市ID',
  `city_name` varchar(20) NOT NULL COMMENT '城市名称',
  `city_person` varchar(20) NOT NULL COMMENT '联系人',
  `emp_id` varchar(20) DEFAULT NULL COMMENT '联系人工号',
  `city_number` varchar(20) NOT NULL COMMENT '联系方式',
  `city_province` varchar(20) NOT NULL COMMENT '所属省份',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='城市信息管理';

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('c1302', '洛阳', '王大虎', '', '12312312345', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1303', '开封', '王大虎', '', '12345678909', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1304', '鹤壁', '王大虎', '', '12312312345', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1305', '信阳', '王大虎', '', '12345678765', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1306', '南阳', '王大虎', '', '12345654321', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1307', '许昌', '王大虎', '', '12323434567', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1308', '漯河', '王大虎', '', '12345665432', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1309', '银川', '王大虎', '', '12345676532', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1310', '南京', '王大虎', '', '12345678908', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1311', '北京', '王大虎', '', '23412312890', '河南', '2017-10-14 09:55:09', '2017-10-19 20:24:15');
INSERT INTO `city` VALUES ('c1312', '长沙', '王大虎', '', '12312312345', '河南', '2017-10-14 09:55:09', '2017-10-14 09:55:09');
INSERT INTO `city` VALUES ('c1313', '长春', '王大虎', '', '12312312345', '吉林', '2017-10-18 20:14:49', '2017-10-18 20:14:49');
INSERT INTO `city` VALUES ('c1314', '武汉', '王大虎', null, '12312345678', '湖北', '2017-10-19 20:27:35', '2017-10-19 20:27:35');
INSERT INTO `city` VALUES ('c1315', '福州', '王大虎', null, '23456789098', '福建', '2017-10-19 20:28:21', '2017-10-19 20:28:21');
INSERT INTO `city` VALUES ('c1316', '银川市', '王大虎', null, '12312312345', '宁夏省', '2017-10-24 16:30:06', '2017-10-24 16:30:06');
INSERT INTO `city` VALUES ('c1317', '兰州市', '王大辉', null, '12334534567', '甘肃省', '2017-10-24 16:37:38', '2017-10-24 16:37:38');
INSERT INTO `city` VALUES ('c1318', '杭州市', '王大辉', null, '12365458545', '浙江省', '2017-10-24 16:46:20', '2017-10-24 16:46:20');
INSERT INTO `city` VALUES ('c1319', '南京市', '王大辉', null, '15454525454', '江苏省', '2017-10-24 16:48:21', '2017-10-24 16:48:21');
INSERT INTO `city` VALUES ('c1320', '西安市', '王大辉', null, '13654525456', '陕西省', '2017-10-24 16:51:42', '2017-10-24 16:51:42');

-- ----------------------------
-- Table structure for claim_order
-- ----------------------------
DROP TABLE IF EXISTS `claim_order`;
CREATE TABLE `claim_order` (
  `claim_id` varchar(50) NOT NULL COMMENT '取货订单编号',
  `site_emp_id` varchar(50) NOT NULL COMMENT '业务员编号',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `user_name` varchar(10) NOT NULL COMMENT '交易人姓名',
  `cargo_id` varchar(50) NOT NULL COMMENT '货物编号',
  `payid` varchar(50) NOT NULL COMMENT '订单交易号',
  `contacts_name` varchar(20) NOT NULL COMMENT '联系人姓名',
  `contacts_phone` varchar(20) NOT NULL COMMENT '联系方式',
  `contacts_addr` varchar(30) NOT NULL COMMENT '联系地址',
  `amount` decimal(5,2) NOT NULL COMMENT '订单金额',
  `paytype` varchar(10) NOT NULL COMMENT '支付方式',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`claim_id`),
  UNIQUE KEY `io_order_payid` (`payid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='取货订单表';

-- ----------------------------
-- Records of claim_order
-- ----------------------------

-- ----------------------------
-- Table structure for code
-- ----------------------------
DROP TABLE IF EXISTS `code`;
CREATE TABLE `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(11) NOT NULL COMMENT '注册手机号',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='手机注册验证码表';

-- ----------------------------
-- Records of code
-- ----------------------------
INSERT INTO `code` VALUES ('35', '18237101977', '557978');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '卡券编号',
  `coupon_type` varchar(10) NOT NULL COMMENT '卡券类型',
  `coupon_number` int(11) NOT NULL COMMENT '卡券发布数量',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `Index 2` (`coupon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='卡券表';

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('1', '月卡', '100', '2017-10-14 11:38:05', '2017-10-14 11:38:05');
INSERT INTO `coupon` VALUES ('2', '月卡', '100', '2017-10-14 11:38:05', '2017-10-14 11:38:05');
INSERT INTO `coupon` VALUES ('3', '年卡', '100', '2017-10-14 11:38:05', '2017-10-14 11:38:05');
INSERT INTO `coupon` VALUES ('4', '周卡', '2000', '2017-10-16 13:50:30', '2017-10-16 13:50:30');
INSERT INTO `coupon` VALUES ('5', '周卡', '200', '2017-10-16 15:05:36', '2017-10-16 15:05:36');
INSERT INTO `coupon` VALUES ('6', 'mkl', '5', '2017-10-16 16:48:30', '2017-10-16 16:48:30');
INSERT INTO `coupon` VALUES ('7', '周卡', '10000', '2017-10-19 14:08:21', '2017-10-19 14:08:21');
INSERT INTO `coupon` VALUES ('8', '年卡', '5000', '2017-10-19 14:26:08', '2017-10-19 14:26:08');
INSERT INTO `coupon` VALUES ('9', '半年卡', '50000', '2017-10-19 14:27:11', '2017-10-19 14:27:11');
INSERT INTO `coupon` VALUES ('10', '两年卡', '1000', '2017-10-19 14:28:18', '2017-10-19 14:28:18');
INSERT INTO `coupon` VALUES ('11', '月卡', '1000', '2017-10-19 14:31:53', '2017-10-19 14:31:53');
INSERT INTO `coupon` VALUES ('12', '三年卡', '500', '2017-10-19 14:35:07', '2017-10-19 14:35:07');
INSERT INTO `coupon` VALUES ('13', '360', '100', '2017-10-23 11:55:44', '2017-10-23 11:55:44');
INSERT INTO `coupon` VALUES ('14', '0', '0', '2017-10-24 19:31:36', '2017-10-24 19:31:36');
INSERT INTO `coupon` VALUES ('15', '11', '11', '2017-10-24 19:46:48', '2017-10-24 19:46:48');
INSERT INTO `coupon` VALUES ('16', '12', '11', '2017-10-24 19:48:10', '2017-10-24 19:48:10');
INSERT INTO `coupon` VALUES ('17', '50', '1000', '2017-10-24 20:03:12', '2017-10-24 20:03:12');
INSERT INTO `coupon` VALUES ('18', '60', '1000', '2017-10-24 20:10:22', '2017-10-24 20:10:22');
INSERT INTO `coupon` VALUES ('19', '70', '1000', '2017-10-24 20:11:35', '2017-10-24 20:11:35');

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `dept_id` varchar(20) NOT NULL COMMENT '部门编号',
  `dept_name` varchar(20) DEFAULT NULL COMMENT '部门名称',
  `dept_location` varchar(20) DEFAULT NULL COMMENT '部门所在地',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_modify` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`dept_id`),
  UNIQUE KEY `dept_name` (`dept_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES ('rsf', '财务部', '河南省郑州市', '2017-10-14 11:39:48', '2017-10-14 11:39:48');
INSERT INTO `dept` VALUES ('rso', '运营总部', '河南省郑州市', '2017-10-14 11:39:48', '2017-10-14 11:39:48');
INSERT INTO `dept` VALUES ('rsp', '人事部', '河南省郑州市', '2017-10-14 11:39:48', '2017-10-14 11:39:48');

-- ----------------------------
-- Table structure for donate
-- ----------------------------
DROP TABLE IF EXISTS `donate`;
CREATE TABLE `donate` (
  `donate_id` tinyint(20) NOT NULL AUTO_INCREMENT COMMENT '捐赠id',
  `donate_name` varchar(50) DEFAULT NULL COMMENT '捐赠者姓名',
  `donate_project` varchar(50) DEFAULT NULL COMMENT '捐赠项目',
  `donate_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '捐赠时间',
  PRIMARY KEY (`donate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='this is a donate table!!';

-- ----------------------------
-- Records of donate
-- ----------------------------
INSERT INTO `donate` VALUES ('1', 'liusongshan,体育设备', null, '2017-10-25 11:33:04');
INSERT INTO `donate` VALUES ('2', 'liusongshanss,爱心午餐', null, '2017-10-25 11:34:06');

-- ----------------------------
-- Table structure for emp_token
-- ----------------------------
DROP TABLE IF EXISTS `emp_token`;
CREATE TABLE `emp_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `emp_id` varchar(50) NOT NULL COMMENT '工号',
  `photo` varchar(50) NOT NULL COMMENT '头像',
  `name` varchar(50) NOT NULL COMMENT '用户名',
  `dept` varchar(50) DEFAULT NULL COMMENT '部门',
  `token` varchar(50) NOT NULL COMMENT '口令',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='员工token表';

-- ----------------------------
-- Records of emp_token
-- ----------------------------
INSERT INTO `emp_token` VALUES ('1', 'rso1', 'default', '何鑫', null, '3d2c7263-9248-4d27-b796-ea5afa77cdf7');
INSERT INTO `emp_token` VALUES ('2', 'rsf2', 'default', '靖品', null, '62404db8-30ca-44b0-84b8-5cc77b4cb433');
INSERT INTO `emp_token` VALUES ('3', 'rsp3', 'default', '武志诚', null, 'bcaecc01-970d-4f0c-aa29-b00312137812');
INSERT INTO `emp_token` VALUES ('4', 'rso4', 'default', '黄佳玮', null, '9e463f30-138b-4314-85f3-4ca1b127595b');
INSERT INTO `emp_token` VALUES ('5', 'rsf3', 'default', '靖品', null, '617121db-9604-48fa-8d19-6dba5ba6684b');
INSERT INTO `emp_token` VALUES ('6', 'rsp2', 'default', '武志诚', null, '07aa26b7-2e33-4e8f-8cfd-2c34c3f640b5');

-- ----------------------------
-- Table structure for express_order
-- ----------------------------
DROP TABLE IF EXISTS `express_order`;
CREATE TABLE `express_order` (
  `express_id` varchar(50) NOT NULL COMMENT '快递订单编号',
  `site_emp_id` varchar(50) NOT NULL COMMENT '员工编号',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `cargo_id` varchar(50) NOT NULL COMMENT '货物编号',
  `express_sendname` varchar(20) NOT NULL COMMENT '寄件人姓名',
  `express_sendphone` varchar(20) NOT NULL COMMENT '寄件人联系方式',
  `express_receivename` varchar(20) NOT NULL COMMENT '收件人姓名',
  `express_receiveaddress` varchar(20) NOT NULL COMMENT '收货地址',
  `express_receivephone` varchar(20) NOT NULL COMMENT '收件人联系方式',
  `express_price` decimal(10,2) unsigned NOT NULL COMMENT '订单价格',
  `express_paytype` varchar(10) NOT NULL COMMENT '订单支付方式',
  `express_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`express_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='快递订单';

-- ----------------------------
-- Records of express_order
-- ----------------------------
INSERT INTO `express_order` VALUES ('20010001', '333', '1', '2001', '刘松山', '13709564512', '德玛', '德玛西亚', '13709564512', '30.00', '微信', '2017-10-11 16:05:33', '2017-10-17 20:16:44');
INSERT INTO `express_order` VALUES ('20010002', '334', '1', '2002', '刘松山', '13546924124', '赵信', '德玛西亚', '13654984215', '40.00', '支付宝', '2017-10-11 17:13:33', '2017-10-17 20:16:41');
INSERT INTO `express_order` VALUES ('20141001', '124', '1', '5000', '刘', '12164565464', '154', '454', '54645645645', '34.00', '支付宝', '2017-10-19 17:50:44', '2017-10-19 17:50:46');

-- ----------------------------
-- Table structure for manager_emp
-- ----------------------------
DROP TABLE IF EXISTS `manager_emp`;
CREATE TABLE `manager_emp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `emp_id` varchar(20) NOT NULL COMMENT '工号//部门编号加序号',
  `password` varchar(20) DEFAULT NULL COMMENT '登录密码',
  `dept_id` varchar(20) NOT NULL COMMENT '部门编号',
  `emp_name` varchar(10) NOT NULL COMMENT '员工名称',
  `emp_phone` varchar(12) NOT NULL COMMENT '员工联系方式',
  `emp_idcard` varchar(18) NOT NULL COMMENT '身份证号',
  `emp_address` varchar(30) NOT NULL COMMENT '员工住址',
  `emp_sex` char(2) NOT NULL COMMENT '性别',
  `emp_job` varchar(10) NOT NULL COMMENT '职位',
  `emp_salary` decimal(8,2) NOT NULL COMMENT '月薪',
  `emp_remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `photo` varchar(50) DEFAULT 'default' COMMENT '头像',
  `emp_hiredate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '入职日期',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='总部员工表';

-- ----------------------------
-- Records of manager_emp
-- ----------------------------
INSERT INTO `manager_emp` VALUES ('1', 'rso1', '1234', 'rso', '何鑫', '13526456544', '455654544123654552', '河南省郑州市', '男', '运营总部经理', '8000.00', '迷之自信', 'default', '2017-10-24 11:34:09', '2017-10-24 11:39:30');
INSERT INTO `manager_emp` VALUES ('2', 'rsp2', '1234', 'rsp', '武志成', '15645236545', '452654523654521255', '河南省周口市', '男', '人事部经理', '8000.00', '好好学习', 'default', '2017-10-24 11:38:42', '2017-10-25 12:05:15');
INSERT INTO `manager_emp` VALUES ('3', 'rsf3', '1234', 'rsf', '靖品', '15654585654', '455265455265458554', '河南省南阳市', '女', 'cfo', '8000.00', '好好学习', 'default', '2017-10-24 11:41:45', '2017-10-24 11:41:45');
INSERT INTO `manager_emp` VALUES ('6', '1', '1', '1', '1', '1', '1', '1', '1', '124', '1.00', '1', '124', '2017-10-24 19:18:40', '2017-10-24 19:18:40');

-- ----------------------------
-- Table structure for recharge_order
-- ----------------------------
DROP TABLE IF EXISTS `recharge_order`;
CREATE TABLE `recharge_order` (
  `recharge_id` varchar(50) NOT NULL COMMENT '充值订单编号',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户编号',
  `recharge_money` decimal(10,0) DEFAULT NULL COMMENT '充值金额',
  `recharge_paytype` varchar(10) DEFAULT NULL COMMENT '充值方式',
  `recharge_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '充值时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`recharge_id`),
  UNIQUE KEY `Index 2` (`recharge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户充值订单明细';

-- ----------------------------
-- Records of recharge_order
-- ----------------------------
INSERT INTO `recharge_order` VALUES ('11', '11', '11', '11', '2017-10-13 13:13:27', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('20171013131445', '11', '100', null, null, '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('20171013131617', '100', '100', null, '2017-10-13 13:16:15', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('20171013131732', '100', '100', '支付宝', '2017-10-13 13:17:29', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('2017101314128', '154', '1000', '微信', '2017-10-13 14:01:25', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('201710131498', '11', '11111', '支付宝', '2017-10-13 14:09:05', '2017-10-14 11:44:24');
INSERT INTO `recharge_order` VALUES ('2017101693412', '12', '1000', '支付宝', '2017-10-16 09:34:14', '2017-10-16 09:34:14');
INSERT INTO `recharge_order` VALUES ('20171017104036', '156', '1000', '支付宝', '2017-10-17 10:40:40', '2017-10-17 10:40:40');
INSERT INTO `recharge_order` VALUES ('2017101710414', '123', '1656', '微信', '2017-10-17 10:41:07', '2017-10-17 10:41:07');
INSERT INTO `recharge_order` VALUES ('20171019141616', '111', '5000', '支付宝', '2017-10-19 14:16:23', '2017-10-19 14:16:23');

-- ----------------------------
-- Table structure for rollover_order
-- ----------------------------
DROP TABLE IF EXISTS `rollover_order`;
CREATE TABLE `rollover_order` (
  `rollover_id` varchar(50) NOT NULL COMMENT '转仓订单编号',
  `cargo_id` varchar(50) DEFAULT NULL COMMENT '货物编号',
  `paytype` varchar(50) DEFAULT NULL COMMENT '支付方式',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户编号',
  `rollover_dest_id` varchar(50) DEFAULT NULL COMMENT '目标站点编号',
  `rollover_start_id` varchar(50) DEFAULT NULL COMMENT '发出站点编号',
  `rollover_price` decimal(10,2) DEFAULT NULL COMMENT '订单金额',
  `rollover_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`rollover_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='转仓订单表';

-- ----------------------------
-- Records of rollover_order
-- ----------------------------
INSERT INTO `rollover_order` VALUES ('8001', '2001', '微信', '1', '1', '003', '215.00', '2017-10-24 10:30:30', '2017-10-24 10:30:31');
INSERT INTO `rollover_order` VALUES ('8002', '2002', '微信', '2', '3', '004', '999.00', '2017-10-24 10:31:04', '2017-10-24 10:31:40');
INSERT INTO `rollover_order` VALUES ('8003', '2003', '支付宝', '3', '4', '005', '1010.00', '2017-10-24 10:31:37', '2017-10-24 10:31:48');
INSERT INTO `rollover_order` VALUES ('8004', '2004', '支付宝', '3', '5', '006', '997.00', '2017-10-24 10:32:12', '2017-10-24 10:33:15');
INSERT INTO `rollover_order` VALUES ('8005', '2005', '微信', '4', '6', '007', '1013.00', '2017-10-24 10:32:20', '2017-10-24 10:33:36');
INSERT INTO `rollover_order` VALUES ('8006', '2006', '支付宝', '5', '7', '008', '1037.00', '2017-10-24 10:32:27', '2017-10-24 10:33:40');

-- ----------------------------
-- Table structure for site
-- ----------------------------
DROP TABLE IF EXISTS `site`;
CREATE TABLE `site` (
  `site_id` int(11) NOT NULL COMMENT '站点id',
  `dept_id` int(11) DEFAULT '12' COMMENT '部门id',
  `site_name` varchar(20) DEFAULT NULL COMMENT '站点名字',
  `site_proniace` varchar(20) DEFAULT NULL COMMENT '所属省份',
  `site_city` varchar(20) DEFAULT NULL COMMENT '所属城市',
  `site_linkman` varchar(20) DEFAULT NULL COMMENT '联系人',
  `site_phone` varchar(20) DEFAULT NULL COMMENT '联系号码',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点表';

-- ----------------------------
-- Records of site
-- ----------------------------
INSERT INTO `site` VALUES ('3', '12', '二七站', '河南省', '郑州市', '比克', '15874859632', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES ('4', '12', '高新站', '河南省', '郑州市', '克林', '15485423564', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES ('5', '12', '高新站', '河南省', '郑州市', '比鲁斯', '14747474747', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES ('6', '12', '二七站', '河南省', '郑州市', '18号', '12365478966', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES ('7', '12', '灌江口', '西牛贺州', '丽江', '哮天犬', '12369854744', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES ('8', '12', '九嶷山', '中土神州', '苍梧之渊', '民', '12365895474', '2017-10-14 08:49:22', '2017-10-17 12:11:11');
INSERT INTO `site` VALUES ('9', '12', '天毒', '东胜神州', '隅有国', '小明', '16587458965', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES ('10', '12', '三川', '北俱芦洲', '雒阳', '八戒·', '15698745698', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES ('21', '12', '二七', '河南', '郑州', '航航那个', '15874589658', '2017-10-14 08:49:22', '2017-10-14 08:49:22');
INSERT INTO `site` VALUES ('23', '10', '高新站', '河南省', '郑州市', '小诚诚', '13719596418', '2017-10-14 08:49:22', '2017-10-14 09:55:31');
INSERT INTO `site` VALUES ('24', '12', '郑州大学', '河南', '郑州', '王大虎', '12312312345', '2017-10-20 16:19:24', '2017-10-20 16:19:24');
INSERT INTO `site` VALUES ('25', '12', '梧桐街', '河南', '郑州', '王大虎', '34534534567', '2017-10-20 16:27:46', '2017-10-20 16:27:46');
INSERT INTO `site` VALUES ('26', '12', '河南工业大学', '河南', '郑州', '王大虎', '12345678909', '2017-10-20 16:35:35', '2017-10-20 16:35:35');
INSERT INTO `site` VALUES ('12354', '12', '地方', '是打发点', '电风扇广东省', '第三方割发代首', '12521252454', '2017-10-24 17:11:54', '2017-10-24 17:12:13');
INSERT INTO `site` VALUES ('123542', '12', '的方式公开', '第三方', '都是个', '十多个', '15454545454', '2017-10-24 17:15:05', '2017-10-24 17:15:05');

-- ----------------------------
-- Table structure for site_emp
-- ----------------------------
DROP TABLE IF EXISTS `site_emp`;
CREATE TABLE `site_emp` (
  `site_emp_id` int(11) unsigned NOT NULL COMMENT '站点员工编号',
  `site_id` int(11) unsigned NOT NULL COMMENT '站点编号',
  `site_emp_name` varchar(10) NOT NULL COMMENT '员工姓名',
  `site_emp_phone` varchar(12) NOT NULL COMMENT '员工联系方法',
  `site_emp_idcard` varchar(20) NOT NULL COMMENT '身份证号',
  `site_emp_address` varchar(30) NOT NULL COMMENT '联系地址',
  `site_emp_sex` char(2) NOT NULL COMMENT '员工性别',
  `site_emp_salary` decimal(8,2) unsigned NOT NULL COMMENT '薪资',
  `site_emp_job` varchar(20) NOT NULL COMMENT '职位',
  `site_emp_remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `mgt_mondified` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`site_emp_id`),
  UNIQUE KEY `site_emp_idcard` (`site_emp_idcard`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点员工表';

-- ----------------------------
-- Records of site_emp
-- ----------------------------
INSERT INTO `site_emp` VALUES ('10', '10', '武志成', '158965235', '10456556', '按时发打发', '男', '10551.20', '丰富', '安慰', '2017-10-15 00:00:00', '2017-10-19 00:00:00');
INSERT INTO `site_emp` VALUES ('13', '2', '1', '1', '1', '1', '1', '1.00', '1', '1', '2017-10-24 19:21:14', '2017-10-24 19:21:14');
INSERT INTO `site_emp` VALUES ('102', '1', '白浅', '1587544747', '156687455896321456', '包子山', '男', '12215.00', '卖烧饼', '卖烧饼喽', '2017-10-20 00:00:00', '2017-10-20 00:00:00');
INSERT INTO `site_emp` VALUES ('103', '1', '夜华', '56987458965', '125888955547856963', '景阳冈', '女', '12365.00', '打虎', '打虎很厉害', '2017-10-20 00:00:00', '2017-10-20 00:00:00');
INSERT INTO `site_emp` VALUES ('106', '1', '刘航', '15487458965', '410225555569999987', '包子山', '男', '125.00', '卖烧饼', '卖烧饼喽', '2017-10-20 00:00:00', '2017-10-20 00:00:00');
INSERT INTO `site_emp` VALUES ('342', '342', '你忙吧', '4324', '242', '423', '42', '24234.00', '234', '234', '2017-10-23 13:07:07', '2017-10-23 13:07:07');
INSERT INTO `site_emp` VALUES ('684', '6', '刘航', '86552', '1654656151', '恶化覅u', '男', '4555.23', '集合', '收款金额非', '2017-10-17 00:00:00', '2017-10-17 00:00:00');
INSERT INTO `site_emp` VALUES ('5566', '3', '冯菁文', '18703615087', '412727198905062356', '是自己的方便交流', '女', '15956.23', '搬运工', '我是一个吃货', '2017-10-16 00:00:00', '2017-10-16 00:00:00');
INSERT INTO `site_emp` VALUES ('5656', '1', '靖品', '15698652314', '412756895655', '南阳市锦州', '女', '3215.23', '卖烧饼', '卖烧饼喽', '2017-10-17 00:00:00', '2017-10-20 00:00:00');
INSERT INTO `site_emp` VALUES ('16264', '9826', '圣斗士', '68416', '55565236', '富国海底世界', '女', '86165.00', '打工人工', '导入', '2017-10-19 00:00:00', '2017-10-19 00:00:00');
INSERT INTO `site_emp` VALUES ('5516516', '5115', '爱妃', '565155', '51598555', '啊色粉sa', '女', '14.00', '561', '51', '2017-10-21 14:53:01', '2017-10-21 14:53:01');

-- ----------------------------
-- Table structure for store_order
-- ----------------------------
DROP TABLE IF EXISTS `store_order`;
CREATE TABLE `store_order` (
  `store_id` varchar(50) NOT NULL COMMENT '存储订单编号',
  `site_emp_id` varchar(50) NOT NULL COMMENT '业务员编号',
  `user_id` varchar(50) NOT NULL COMMENT '用户编号',
  `user_name` varchar(10) NOT NULL COMMENT '交易人姓名',
  `cargo_id` varchar(50) NOT NULL COMMENT '货物编号',
  `contacts_name` varchar(20) NOT NULL COMMENT '联系人姓名',
  `contacts_phone` varchar(20) NOT NULL COMMENT '联系方式',
  `contacts_addr` varchar(30) NOT NULL COMMENT '联系地址',
  `amount` decimal(5,2) NOT NULL COMMENT '订单金额',
  `paytype` varchar(10) NOT NULL COMMENT '支付方式',
  `store_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '下单时间',
  `mgt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `cargo_type` varchar(50) NOT NULL COMMENT '物品类型',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `chose_area` varchar(50) NOT NULL COMMENT '选择地区'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='存储订单表';

-- ----------------------------
-- Records of store_order
-- ----------------------------
INSERT INTO `store_order` VALUES ('9001', '5656', '1001', '夜华', '2001', '白浅', '15138685092', '青邱', '999.99', '微信', '2017-10-24 10:52:17', '2017-10-24 10:52:19', '桃花醉', '易碎品', '郑州');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `user_nickname` varchar(20) NOT NULL COMMENT '昵称',
  `user_name` varchar(10) DEFAULT NULL COMMENT '名字',
  `user_phone` varchar(12) NOT NULL COMMENT '手机',
  `user_email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `user_photo` varchar(50) DEFAULT 'default' COMMENT '用户头像',
  `user_pwd` varchar(20) NOT NULL COMMENT '密码',
  `user_point` int(11) unsigned DEFAULT NULL COMMENT '积分',
  `mgt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `mgt_mondified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_nickname` (`user_nickname`),
  UNIQUE KEY `user_phone` (`user_phone`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'honker丶boy', '刘松山', '15890197161', '937661655@qq.com', 'default', '1314', '1000', '2017-10-14 09:13:04', '2017-10-20 09:32:38');
INSERT INTO `user` VALUES ('4', '小新aaaa', null, '18737832326', null, 'default', '123456', null, '2017-10-23 12:10:30', '2017-10-23 12:10:30');
INSERT INTO `user` VALUES ('5', '冯静文aaa', null, '18703615027', null, 'default', '123456', null, '2017-10-23 12:13:57', '2017-10-23 12:13:57');
INSERT INTO `user` VALUES ('6', '武志诚', null, '15138938657', null, 'default', '1234', null, '2017-10-23 19:37:35', '2017-10-23 19:37:35');
INSERT INTO `user` VALUES ('7', 'xiaopang', null, '13140001897', null, 'default', '1234', null, '2017-10-25 11:59:13', '2017-10-25 11:59:13');

-- ----------------------------
-- Table structure for user_token
-- ----------------------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户编号',
  `photo` varchar(50) NOT NULL COMMENT '头像',
  `nickname` varchar(50) NOT NULL COMMENT '昵称',
  `token` varchar(50) NOT NULL COMMENT '口令',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户token表';

-- ----------------------------
-- Records of user_token
-- ----------------------------
INSERT INTO `user_token` VALUES ('5', '1', 'default', 'honker丶boy', '79f03743-1309-4005-b900-3a56ddfcb26f');
INSERT INTO `user_token` VALUES ('6', '4', 'default', '小新aaaa', '53164ad1-c814-486e-ad05-e327c8bf34bc');
INSERT INTO `user_token` VALUES ('7', '5', 'default', '冯静文aaa', 'b823d17f-3998-4491-9978-cfd859f38f7a');
INSERT INTO `user_token` VALUES ('8', '6', 'default', '武志诚', 'b3e39388-a706-4b06-ad08-5f4c74870263');
INSERT INTO `user_token` VALUES ('9', '7', 'default', 'xiaopang', '7fc842ab-cd0e-43ea-8e66-ac10b74d6cae');
