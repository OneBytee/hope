package com.lh.common.exception;

import com.lh.common.enums.ReturnCodeEnum;
import lombok.Getter;

/**
 * 自定义异常类
 */
@Getter
public class HopeException extends RuntimeException {

    private static final long serialVersionUID = -7864604160467181941L;

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 无参默认构造UNSPECIFIED
     */
    public HopeException() {
        super();
    }

    /**
     * 指定错误码构造通用异常
     *
     * @param returnCodeEnum 错误码
     */
    public HopeException(final ReturnCodeEnum returnCodeEnum) {
        super(returnCodeEnum.getMessage());
        this.errorCode = returnCodeEnum.getCode();
    }

    /**
     * 指定详细描述构造通用异常
     *
     * @param message 详细描述
     */
    public HopeException(final String message) {
        super(message);
        this.errorCode = ReturnCodeEnum.SYSTEM_ERROR.getCode();
    }

    /**
     * 构造通用异常
     *
     * @param errorCode 错误码
     * @param message   详细描述
     */
    public HopeException(final String errorCode, final String message) {
        super(message);
        this.errorCode = errorCode;
    }

}
