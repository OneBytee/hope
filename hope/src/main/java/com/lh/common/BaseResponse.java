package com.lh.common;

import com.lh.common.enums.ReturnCodeEnum;
import lombok.*;

@Data
@Builder
public class BaseResponse<T> {

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 返回码
     */
    private String code;

    /**
     * 返回信息
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;

    /**
     * 无参构造
     */
    private BaseResponse() {
    }

    /**
     * 全参构造
     */
    private BaseResponse(Boolean success, String code, String message, T data) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> BaseResponse<T> create(Boolean success, String code, String message, T data) {
        return new BaseResponse<>(success, code, message, data);
    }

    public static <T> BaseResponse<T> create(Boolean success, ReturnCodeEnum returnCodeEnum, T data) {
        return create(success, returnCodeEnum.getCode(), returnCodeEnum.getMessage(), data);
    }

    public static <T> BaseResponse<T> success(T data) {
        return create(true, ReturnCodeEnum.SUCCESS, data);
    }

    public static <T> BaseResponse<T> success(String code, String message, T data) {
        return create(true, code, message, data);
    }

    public static <T> BaseResponse<T> success(String code, String message) {
        return create(true, code, message, null);
    }

    public static <T> BaseResponse<T> success(String code, T data) {
        return create(true, code, null, data);
    }

    public static <T> BaseResponse<T> success(T data, String message) {
        return create(true, ReturnCodeEnum.SUCCESS.getCode(), message, data);
    }

    public static <T> BaseResponse<T> error(ReturnCodeEnum returnCodeEnum) {
        return create(false, returnCodeEnum, null);
    }

    public static <T> BaseResponse<T> error(String code, String message) {
        return create(false, code, message, null);
    }

}
