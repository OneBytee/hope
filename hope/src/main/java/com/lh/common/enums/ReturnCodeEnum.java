package com.lh.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 返回码
 */
@Getter
@AllArgsConstructor
public enum ReturnCodeEnum {

    SUCCESS("000000", "交易成功"),

    PARAM_MISSING("100001", "参数缺失"),
    CONFIG_MISSING("100002", "配置信息缺失"),

    BIZ_ERROR("900001", "业务异常"),
    API_ERROR("900002", "第三方接口异常"),
    SYSTEM_ERROR("999999", "系统异常");

    /**
     * 返回码
     */
    private String code;

    /**
     * 返回信息
     */
    private String message;

}
