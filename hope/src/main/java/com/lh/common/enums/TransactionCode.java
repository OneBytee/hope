package com.lh.common.enums;

/**
 * @author Administrator
 * @Auther Liuhang
 */
public enum TransactionCode {

    SUCCESS("200", "交易成功"),
    NO_AUTHORITY("403", "无权访问"),
    NO_RESOURCES("404", "资源找不到"),
    ERROR("500", "交易失败"),
    NO_LOGION("1000001", "未登录");
    private String code;
    private String msg;

    TransactionCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
