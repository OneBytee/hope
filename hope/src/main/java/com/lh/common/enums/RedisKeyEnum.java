package com.lh.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RedisKeyEnum {

    CUST_QUERY_THREAD_COUNT("CUST_QUERY_THREAD_COUNT", "客户查询线程数");

    private String code;
    private String msg;

}
