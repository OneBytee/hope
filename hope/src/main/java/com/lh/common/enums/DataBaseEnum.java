package com.lh.common.enums;

public enum DataBaseEnum {
    MYSQL("mysql", "mysql数据库"),
    ORACLE("oracle", "oracle数据库"),
    SQLSERVER("sqlserver", "sqlserver数据库"),
    POSTGRESQL("postgresql", "postgresql数据库");
    private String code;

    private String name;

    DataBaseEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
