package com.lh.entity;

import lombok.Data;

import java.util.Date;

@Data
public class CustQueryRecord {

    private Integer id;

    private String recordId;

    private Integer page;

    private String remark;

    private String busiDate;

    private Date inputTime;

    private Date updateTime;

}