package com.lh.entity.sys;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class SysRolePermission implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 角色编号
     */
    private String roleId;

    /**
     * 资源编号
     */
    private Integer permissionId;

    /**
     * 创建人
     */
    private String creadtedBy;

    /**
     * 创建时间
     */
    private Date creadtedTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

}