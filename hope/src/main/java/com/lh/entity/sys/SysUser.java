package com.lh.entity.sys;

import com.lh.entity.common.BaseEntity;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@ToString
public class SysUser extends BaseEntity implements Serializable {
    /**
     * 用户编号
     */
    private Integer id;

    /**
     * 昵称
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户手机号
     */
    private String userPhone;

    /**
     * 用户邮箱
     */
    private String userEmail;

    /**
     * 用户状态 0、正常 1、锁定 2、注销
     */
    private Integer status;

    /**
     * 用户头像
     */
    private String userPhoto;

    /**
     * 密码
     */
    private String password;

    /**
     * 盐
     */
    private String salt;

    /**
     * 积分
     */
    private Integer userPoint;

    /**
     * 添加时间
     */
    private Date createdTime;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 修改人
     */
    private String updatedBy;

    /**
     * 用户角色集
     */
    private List<SysRole> sysRoles;


    private String createdTimeStr;

    private String updatedTimeStr;

}