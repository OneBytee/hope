package com.lh.entity.sys;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@ToString
public class SysRole implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 角色编号
     */
    private String roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 状态 0 失效， 1生效
     */
    private Integer status;

    /**
     * 描述
     */
    private String description;

    /**
     * 创建人
     */
    private String creadtedBy;

    /**
     * 创建时间
     */
    private Date creadtedTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 角色对应的资源集
     */
    private List<SysPermission> sysPermissions;

}