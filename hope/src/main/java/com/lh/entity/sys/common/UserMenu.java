package com.lh.entity.sys.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.List;

@Data
public class UserMenu {

    /**
     * 资源编号
     */
    @JSONField(serialize = false)
    @JsonIgnore
    private Integer id;

    /**
     * 父级编号
     */
    @JSONField(serialize = false)
    @JsonIgnore
    private Integer parentId;

    /**
     * 菜单级别
     */
    @JSONField(serialize = false)
    @JsonIgnore
    private Integer rank;

    /**
     * 菜单路径
     */
    private String index;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 菜单名称
     */
    private String title;

    /**
     * 子菜单
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<UserMenu> subs;

}
