package com.lh.entity.sys;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@ToString
public class SysPermission implements Serializable {
    /**
     * 资源编号
     */
    private Integer id;

    /**
     * 父级编号
     */
    private Integer parentId;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 资源URL
     */
    private String url;

    /**
     * 图标
     */
    private String icon;

    /**
     * 类型 0 菜单 1按钮
     */
    private Integer type;

    /**
     * 资源标示
     */
    private String permission;

    /**
     * 菜单等级 1、一级菜单 2、二级菜单 3、三级菜单
     */
    private Integer menuRank;

    /**
     * 状态 0 失效， 1生效
     */
    private Integer status;

    /**
     * 创建人
     */
    private String creadtedBy;

    /**
     * 创建时间
     */
    private Date creadtedTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

}