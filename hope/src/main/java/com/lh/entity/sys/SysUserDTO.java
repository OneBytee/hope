package com.lh.entity.sys;

import com.lh.entity.common.PageBase;
import lombok.Data;

@Data
public class SysUserDTO extends PageBase {

    /**
     * 昵称
     */
    private String userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户手机号
     */
    private String userPhone;

    /**
     * 用户邮箱
     */
    private String userEmail;

    /**
     * 用户状态 0、正常 1、锁定 2、注销
     */
    private Integer status;
}
