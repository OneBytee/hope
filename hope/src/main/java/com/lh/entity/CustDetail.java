package com.lh.entity;

import lombok.Data;

import java.util.Date;

@Data
public class CustDetail {

    private Integer id;

    private String custId;

    private String city;

    private String name;

    private String tel;

    private Integer age;

    private Date inputTime;

    private Date updateTime;

    public CustDetail() {
    }

    public CustDetail(String custId, String name, String tel) {
        this.custId = custId;
        this.name = name;
        this.tel = tel;
    }

}