package com.lh.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustInfo {

    private Integer id;

    private String custId;

    private Byte status;

    private String remark;

    private Date inputTime;

    private Date updateTime;

    public CustInfo(String custId, Byte status, String remark) {
        this.custId = custId;
        this.status = status;
        this.remark = remark;
    }

    public CustInfo(Integer id, Byte status, String remark) {
        this.id = id;
        this.status = status;
        this.remark = remark;
    }
}