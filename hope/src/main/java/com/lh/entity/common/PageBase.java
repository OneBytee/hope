package com.lh.entity.common;

import lombok.ToString;

@ToString
public class PageBase {

    /**
     * 页号
     */
    private Integer pageIndex;

    /**
     * 每页条数
     */
    private Integer pageSize;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        if (pageIndex == null) {
            this.pageIndex = 1;
        } else {
            this.pageIndex = pageIndex;
        }
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        if (pageSize == null) {
            this.pageSize = 10;
        } else {
            this.pageSize = pageSize;
        }
    }
}
