package com.lh.entity.common;


import com.lh.common.enums.TransactionCode;

public class Result<T> {

    private String code;

    private String msg;

    private T data;

    private boolean success;

    public static Result ok(Object data) {
        return ok(TransactionCode.SUCCESS.getCode(), TransactionCode.SUCCESS.getMsg(), true, data);
    }

    public static Result ok() {
        return new Result<>(TransactionCode.SUCCESS.getCode(), TransactionCode.SUCCESS.getMsg(), true);
    }

    public static Result ok(String msg) {
        return new Result<>(TransactionCode.SUCCESS.getCode(), msg, true);
    }

    public static Result ok(String code, String msg, boolean success, Object data) {
        return new Result<>(code, msg, success, data);
    }

    public static Result error() {
        return new Result<>(TransactionCode.ERROR.getCode(), TransactionCode.ERROR.getMsg(), false);
    }

    public static Result error(String msg) {
        return new Result<>(TransactionCode.ERROR.getCode(), msg, false);
    }

    public static Result error(String code, String msg) {
        return new Result<>(code, msg, false);
    }

    public Result() {
    }

    public Result(String code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success = success;
    }

    public Result(String code, String msg, boolean success, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
