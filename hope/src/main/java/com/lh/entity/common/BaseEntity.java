package com.lh.entity.common;

import lombok.Data;

@Data
public class BaseEntity {

    /**
     * 页号
     */
    private Integer pageIndex;

    /**
     * 每页条数
     */
    private Integer pageSize;
}
