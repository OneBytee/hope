package com.lh.entity.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageModel<T> {

    /**
     * 页码
     */
    private Integer pageIndex;

    /**
     * 页距（条/页）
     */
    private Integer pageSize;

    /**
     * 总条数
     */
    private Integer total;

    /**
     * 数据
     */
    private List<T> data;

}
