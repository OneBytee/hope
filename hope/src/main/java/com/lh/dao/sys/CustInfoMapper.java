package com.lh.dao.sys;

import com.lh.entity.CustInfo;

import java.util.List;

public interface CustInfoMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(CustInfo record);

    int insertSelective(CustInfo record);

    CustInfo selectByPrimaryKey(Integer id);

    List<CustInfo> selectCustInfoList();

    List<CustInfo> selectList();

    int selectCustInfoCount();

    int updateByPrimaryKeySelective(CustInfo record);

    int updateByPrimaryKey(CustInfo record);
}