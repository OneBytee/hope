package com.lh.dao.sys;

import com.lh.entity.CustQueryRecord;
import org.apache.ibatis.annotations.Param;

public interface CustQueryRecordMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(CustQueryRecord record);

    int insertSelective(CustQueryRecord record);

    CustQueryRecord selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CustQueryRecord record);

    int updateByPrimaryKey(CustQueryRecord record);

    int selectMaxPageByBusiDate(@Param("busiDate") String busiDate);
}