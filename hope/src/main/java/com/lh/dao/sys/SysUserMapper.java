package com.lh.dao.sys;

import com.lh.entity.sys.SysRole;
import com.lh.entity.sys.SysUser;

import java.util.List;

public interface SysUserMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    List<SysUser> selectAll();

    SysUser selectByUserId(String userId);

    List<SysUser> selectUserList(SysUser user);

}