package com.lh.dao.sys;

import com.lh.entity.CustDetail;

public interface CustDetailMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(CustDetail record);

    int insertSelective(CustDetail record);

    CustDetail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(CustDetail record);

    int updateByPrimaryKey(CustDetail record);
}