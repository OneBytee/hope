/*
package com.lh.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lh.dao.sys.CustInfoMapper;
import com.lh.entity.CustInfo;
import com.lh.common.enums.RedisKeyEnum;
import com.lh.service.CustInfoService;
import com.lh.service.task.CustQueryOneThread;
import com.lh.service.task.CustThreadPoolExecutor;
import com.lh.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service("custInfoOneServiceImpl")
public class CustInfoOneServiceImpl implements CustInfoService {

    private static final int PAGE_SIZE = 30;
    private static final int QUERY_SIZE = 10;

    @Resource
    private CustInfoMapper custInfoMapper;

    @Resource
    private RedisUtil redisUtil;

    @Override
    public void queryJob() {
        try {
            log.info("==============cust query task begin!=================");
            int i = custInfoMapper.selectCustInfoCount();
            int pageCount = i / PAGE_SIZE + (i % PAGE_SIZE > 0 ? 1 : 0);
            int flag = 1;
            redisUtil.set(RedisKeyEnum.CUST_QUERY_THREAD_COUNT.getCode(), "0");
            while (flag <= pageCount) {
                String s = redisUtil.get(RedisKeyEnum.CUST_QUERY_THREAD_COUNT.getCode());
                if ("0".equals(s)) {
                    this.query();
                } else if ("-1".equals(s)) {
                    break;
                } else {
                    Thread.sleep(1000);
                }
            }
            log.info("==============cust query task end !=================");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("==============cust query task exception! e={}=================", e);
        }
    }

    private void query() {
        Page page = PageHelper.startPage(1, PAGE_SIZE);
        List<CustInfo> list = custInfoMapper.selectCustInfoList();
        if (list == null || list.size() == 0) {
            redisUtil.set(RedisKeyEnum.CUST_QUERY_THREAD_COUNT.getCode(), "-1");
            return;
        }
        int size = list.size();
        int m = size / QUERY_SIZE;
        int n = size % QUERY_SIZE;
        int threadCount = m + (n > 0 ? 1 : 0);
        redisUtil.set(RedisKeyEnum.CUST_QUERY_THREAD_COUNT.getCode(), String.valueOf(threadCount));
        if (m > 0) {
            for (int i = 0; i < m; i++) {
                List<CustInfo> collect = list.stream().skip(QUERY_SIZE * i).limit(QUERY_SIZE).collect(Collectors.toList());
                CustThreadPoolExecutor.execute(new CustQueryOneThread(collect));
            }
        }
        if (n > 0) {
            List<CustInfo> collect = list.stream().skip(QUERY_SIZE * m).collect(Collectors.toList());
            CustThreadPoolExecutor.execute(new CustQueryOneThread(collect));
        }
    }
}
*/
