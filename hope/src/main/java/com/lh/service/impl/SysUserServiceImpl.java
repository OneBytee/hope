package com.lh.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lh.common.BaseResponse;
import com.lh.common.enums.ReturnCodeEnum;
import com.lh.dao.sys.SysPermissionMapper;
import com.lh.dao.sys.SysRoleMapper;
import com.lh.dao.sys.SysUserMapper;
import com.lh.entity.sys.SysPermission;
import com.lh.entity.sys.SysRole;
import com.lh.entity.sys.SysUser;
import com.lh.entity.common.PageModel;
import com.lh.entity.common.Result;
import com.lh.entity.sys.SysUserDTO;
import com.lh.service.SysUserService;
import com.lh.utils.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.IntSupplier;

@Slf4j
@Service("userServiceImpl")
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Resource
    private SysPermissionMapper sysPermissionMapper;

    @Resource
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public Result<PageModel<SysUser>> selectUserList(SysUser user) {
        log.info("查询用户列表开始！user={}", JSON.toJSON(user));
        Page page = PageHelper.startPage(user.getPageIndex(), user.getPageSize());
        List<SysUser> users = sysUserMapper.selectUserList(user);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        users.forEach(a -> {
            a.setCreatedTimeStr(sdf.format(a.getCreatedTime()));
            a.setUpdatedTimeStr(sdf.format(a.getUpdatedTime()));
        });
        PageModel pageModel = new PageModel();
        pageModel.setPageIndex(page.getPageNum());
        pageModel.setPageSize(page.getPageSize());
        pageModel.setTotal((int) page.getTotal());
        pageModel.setData(users);
        return Result.ok(pageModel);
    }

    /**
     * 根据用户id 删除用户（仅支持逻辑删除）
     *
     * @param id 用户id
     * @return
     */
    @Override
    public Result<String> deleteUserById(Integer id) {
        log.info("删除用户开始！id={}", id);
        SysUser user = new SysUser();
        user.setId(id);
        user.setStatus(3);
        int i = sysUserMapper.updateByPrimaryKeySelective(user);
        if (i > 0) {
            return Result.ok("删除用户成功！");
        } else {
            return Result.error("删除用户失败！");
        }
    }

    /**
     * 修改用户信息
     *
     * @param sysUser
     * @return
     */
    @Override
    public Result<String> editUserById(SysUser sysUser) {
        log.info("修改用户信息开始 sysUser={}", sysUser);
        return this.editOrAddUser("修改用户信息", sysUser, () -> sysUserMapper.updateByPrimaryKeySelective(sysUser));
    }

    /**
     * 新增用户
     *
     * @param sysUser
     * @return
     */
    @Override
    public Result<String> addUser(SysUser sysUser) {
        log.info("新增用户开始！sysUser={}", sysUser);
        sysUser.setPassword("1234");
        sysUser.setStatus(0);
        sysUser.setUserPhoto("default");
        return this.editOrAddUser("新增用户", sysUser, () -> sysUserMapper.insertSelective(sysUser));
    }

    @Override
    public SysUser selectSysUserByUserId(String userId) {
        SysUser sysUser = sysUserMapper.selectByUserId(userId);
        if (sysUser == null) {
            return null;
        }
        List<SysRole> sysRoles = sysRoleMapper.selectSysRoleListByUserId(sysUser.getUserId());
        if (sysRoles != null && sysRoles.size() > 0) {
            sysUser.setSysRoles(sysRoles);
            for (SysRole sysRole : sysRoles) {
                List<SysPermission> sysPermissions = sysPermissionMapper.selectSysPermissionListByRoleId(sysRole.getRoleId());
                if (sysPermissions != null && sysPermissions.size() > 0) {
                    sysRole.setSysPermissions(sysPermissions);
                }
            }
        }
        return sysUser;
    }

    @Override
    public SysUser selectSysTest(String userId) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        SysUserMapper mapper = sqlSession.getMapper(SysUserMapper.class);
        SysUser sysUser = mapper.selectByUserId(userId);
        return sysUser;
    }

    @Override
    @Transactional
    public void testBatch() {
        SysUser sysUser = new SysUser();
        sysUser.setUserId("normalbefore");
        sysUser.setUserPhone("172727284");
        sysUser.setStatus(1);
        sysUser.setPassword("123456normal");
        sysUserMapper.insertSelective(sysUser);
        List<SysUser> test = this.getUserList("TestA2", "182378");
        this.saveList(test);
        SysUser sysUser2 = new SysUser();
        sysUser2.setUserId("normalafter");
        sysUser2.setUserPhone("1728134");
        sysUser2.setStatus(1);
        sysUser2.setPassword("123456normal");
        sysUserMapper.insertSelective(sysUser2);
        int i = 1 / 0;
    }

    @Override
    public BaseResponse<PageModel<SysUser>> queryUserList(SysUserDTO dto) {
        try {
            SysUser sysUser = new SysUser();
            BeanUtils.copyProperties(dto, sysUser);
            PageModel<SysUser> pageModel = PageUtil.query(dto.getPageIndex(), dto.getPageSize(), () -> sysUserMapper.selectUserList(sysUser));
            return BaseResponse.success(pageModel);
        } catch (Exception e) {
            log.error("SysUserServiceImpl queryUserList exception! dto={}", dto, e);
            return BaseResponse.error(ReturnCodeEnum.SYSTEM_ERROR);
        }
    }

    @Transactional
    public void saveList(List<SysUser> list) {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            SysUserMapper mapper = sqlSession.getMapper(SysUserMapper.class);
            for (SysUser sysUser : list) {
                mapper.insertSelective(sysUser);
            }
            sqlSession.commit();
            sqlSession.clearCache();
        } catch (Exception e) {
            log.error("发生异常！e={}", e);
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    private List<SysUser> getUserList(String name, String phone) {
        List<SysUser> result = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            SysUser sysUser = new SysUser();
            sysUser.setUserId(name + i);
            sysUser.setUserPhone(phone + i);
            sysUser.setStatus(1);
            sysUser.setPassword("123456" + i);
            result.add(sysUser);
        }
        return result;
    }

    private Result<String> editOrAddUser(String msg, SysUser sysUser, IntSupplier intSupplier) {
        try {
            // 校验昵称是否唯一
            if (StringUtils.isBlank(sysUser.getUserId())) {
                return Result.error("用户编号不能为空！");
            }
            if (!this.checkPropertyForUpdateAndAdd("userId", sysUser.getUserId(), sysUser.getId())) {
                return Result.error("该用户编号已被使用！");
            }

            //校验手机号是否唯一
            if (StringUtils.isBlank(sysUser.getUserPhone())) {
                return Result.error("用户手机号不能为空！");
            }
            if (!this.checkPropertyForUpdateAndAdd("userPhone", sysUser.getUserPhone(), sysUser.getId())) {
                return Result.error("该手机号已被使用！");
            }
            int i = intSupplier.getAsInt();
            if (i > 0) {
                return Result.ok(msg + "成功！");
            } else {
                return Result.ok(msg + "失败！");
            }
        } catch (Exception e) {
            log.error(msg + "时发生异常！e={}", e);
            return Result.error(msg + "时发生异常！");
        }
    }

    /**
     * 新增删除是对参数进行校验
     *
     * @param key   参数名
     * @param value 参数值
     * @param id    用户主键  null时新增,有值时为修改
     * @return
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    private boolean checkPropertyForUpdateAndAdd(String key, String value, Integer id) throws InvocationTargetException, IllegalAccessException {
        SysUser param = new SysUser();
        BeanUtils.setProperty(param, key, value);
        List<SysUser> users = sysUserMapper.selectUserList(param);
        if (users != null && users.size() > 0) {
            if (id != null) {//修改
                long count = users.stream().filter(a -> !a.getId().equals(id)).count();
                return !(count > 0);
            } else {//新增
                return false;
            }
        } else {
            return true;
        }
    }

}
