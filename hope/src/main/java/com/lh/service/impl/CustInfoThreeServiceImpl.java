package com.lh.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lh.dao.sys.CustDetailMapper;
import com.lh.dao.sys.CustInfoMapper;
import com.lh.dao.sys.CustQueryRecordMapper;
import com.lh.entity.CustDetail;
import com.lh.entity.CustInfo;
import com.lh.entity.CustQueryRecord;
import com.lh.service.CustInfoService;
import com.lh.service.task.CustQueryThreeThread;
import com.lh.service.task.CustQueryTwoThread;
import com.lh.service.task.CustThreadPoolExecutor;
import com.lh.utils.BatchInsertUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service("custInfoThreeServiceImpl")
public class CustInfoThreeServiceImpl implements CustInfoService {

    private static final int PAGE_SIZE = 30;
    private static final int QUERY_SIZE = 10;

    @Resource
    private CustInfoMapper custInfoMapper;

    @Resource
    private CustQueryRecordMapper custQueryRecordMapper;

    @Override
    public void queryJob() {
        try {
            log.info("==============cust query task begin!=================");
            Semaphore semaphore = new Semaphore(3);
            int i = custInfoMapper.selectCustInfoCount();
            int pageCount = i / PAGE_SIZE + (i % PAGE_SIZE > 0 ? 1 : 0);
            int flag = 1;
            while (flag <= pageCount) {
                Page page = PageHelper.startPage(flag, PAGE_SIZE);
                List<CustInfo> list = custInfoMapper.selectList();
                if (list == null || list.size() == 0) {
                    break;
                }
                this.query(list, semaphore);
                this.saveRecord(flag, list.size());
                flag++;
            }
            log.info("==============cust query task end !=================");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("==============cust query task exception! e={}=================", e);
        }
    }

    @Override
    public void testInsert() {
        AtomicInteger atomic = new AtomicInteger(10);
        List<CustDetail> list = Arrays.asList(
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get()),
                new CustDetail("202007130000" + atomic.incrementAndGet(), "小麻花" + atomic.get(), "18729872910" + atomic.get())
        );
        BatchInsertUtil.batchInsert(list, CustDetailMapper.class, CustDetailMapper::insert, "sqlSessionFactory");
    }

    /**
     * 查询
     *
     * @param list
     * @param semaphore
     * @throws InterruptedException
     */
    private void query(List<CustInfo> list, Semaphore semaphore) throws InterruptedException {
        int size = list.size();
        int m = size / QUERY_SIZE;
        int n = size % QUERY_SIZE;

        if (m > 0) {
            for (int i = 0; i < m; i++) {
                List<CustInfo> collect = list.stream().skip(QUERY_SIZE * i).limit(QUERY_SIZE).collect(Collectors.toList());
                // 获取许可
                semaphore.acquire();
                CustThreadPoolExecutor.execute(new CustQueryThreeThread(collect, semaphore));
            }
        }
        if (n > 0) {
            List<CustInfo> collect = list.stream().skip(QUERY_SIZE * m).collect(Collectors.toList());
            CustThreadPoolExecutor.execute(new CustQueryThreeThread(collect, semaphore));
        }
    }

    /**
     * 保存记录
     *
     * @param page
     * @param size
     */
    private void saveRecord(int page, int size) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String busiDate = sdf.format(new Date());
        CustQueryRecord record = new CustQueryRecord();
        record.setRecordId(busiDate + "_" + page);
        record.setPage(page);
        record.setBusiDate(busiDate);
        record.setRemark(String.valueOf(size));
        custQueryRecordMapper.insertSelective(record);
    }
}
