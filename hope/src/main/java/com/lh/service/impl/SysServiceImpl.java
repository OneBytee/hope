package com.lh.service.impl;

import com.lh.dao.sys.SysPermissionMapper;
import com.lh.dao.sys.SysRoleMapper;
import com.lh.dao.sys.SysRolePermissionMapper;
import com.lh.entity.common.Result;
import com.lh.entity.sys.SysPermission;
import com.lh.entity.sys.SysRole;
import com.lh.entity.sys.SysRolePermission;
import com.lh.entity.sys.common.UserMenu;
import com.lh.service.SysService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SysServiceImpl implements SysService {

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Resource
    private SysPermissionMapper sysPermissionMapper;

    @Resource
    private SysRolePermissionMapper sysRolePermissionMapper;

    /**
     * 1、获取当前用户的所有角色
     * 2、查询所有可用菜单
     * 3、根据用户角色筛选用户所有可见菜单
     * 4、组装菜单
     *
     * @param userId
     * @return
     */
    @Override
    public Result<List<UserMenu>> selectUserMenu(String userId) {
        List<SysRole> sysRoles = sysRoleMapper.selectSysRoleListByUserId(userId);
        if (sysRoles == null || sysRoles.size() == 0) {
            log.error("根据用户编号查询用户菜单时，用户角色信息为空! userId={}", userId);
            return Result.ok(Collections.EMPTY_LIST);
        }
        SysPermission sysPermission = new SysPermission();
        sysPermission.setStatus(1);
        List<SysPermission> sysPermissions = sysPermissionMapper.selectSysPermissionListByParams(sysPermission);
        if (sysPermissions == null || sysPermissions.size() == 0) {
            log.error("根据用户编号查询用户菜单时，可见菜单为空! userId={}", userId);
            return Result.ok(Collections.EMPTY_LIST);
        }
        List<UserMenu> menuList = sysPermissions.stream().map(a -> {
            UserMenu userMenu = new UserMenu();
            userMenu.setId(a.getId());
            userMenu.setParentId(a.getParentId());
            userMenu.setIcon(a.getIcon());
            userMenu.setIndex(a.getUrl());
            userMenu.setTitle(a.getName());
            userMenu.setRank(a.getMenuRank());
            return userMenu;
        }).collect(Collectors.toList());
        List<UserMenu> userMenus = this.checkUserMenu(menuList, sysRoles.stream().map(SysRole::getRoleId).collect(Collectors.toList()));
        if (userMenus != null && userMenus.size() > 0) {
            Map<Integer, List<UserMenu>> collect = userMenus.stream().collect(Collectors.groupingBy(UserMenu::getRank));
            /** 一级菜单集*/
            List<UserMenu> userMenus1 = collect.get(1);
            /** 一级菜单集*/
            List<UserMenu> userMenus2 = collect.get(2);
            /** 一级菜单集*/
            List<UserMenu> userMenus3 = collect.get(3);
            return Result.ok(this.assembleUserMenu(userMenus1, this.assembleUserMenu(userMenus2, userMenus3)));
        } else {
            log.error("根据用户编号查询用户菜单时，可见菜单为空! userId={}", userId);
            return Result.ok(Collections.EMPTY_LIST);
        }
    }

    @Override
    public Result<List<String>> selectUserPermission(String userId) {
        List<SysRole> sysRoles = sysRoleMapper.selectSysRoleListByUserId(userId);
        if (sysRoles == null || sysRoles.size() == 0) {
            log.error("根据用户编号查询用户菜单时，用户角色信息为空! userId={}", userId);
            return Result.ok(Collections.EMPTY_LIST);
        }
        SysPermission sysPermission = new SysPermission();
        sysPermission.setType(0);
        sysPermission.setStatus(1);
        List<SysPermission> sysPermissions = sysPermissionMapper.selectSysPermissionListByParams(sysPermission);
        if (sysPermissions == null || sysPermissions.size() == 0) {
            log.error("根据用户编号查询用户菜单时，可见菜单为空! userId={}", userId);
            return Result.ok(Collections.EMPTY_LIST);
        }
        return Result.ok(sysPermissions.stream().map(a -> "/" + a.getUrl()).collect(Collectors.toList()));
    }

    /**
     * 根据角色集筛选可见菜单
     *
     * @param list  菜单集合
     * @param roles 角色集合
     * @return
     */
    private List<UserMenu> checkUserMenu(List<UserMenu> list, List<String> roles) {
        List<UserMenu> result = list.stream().filter(a -> {
            SysRolePermission params = new SysRolePermission();
            params.setPermissionId(a.getId());
            List<SysRolePermission> sysRolePermissions = sysRolePermissionMapper.selectSysRolePermissionByParams(params);
            if (sysRolePermissions != null && sysRolePermissions.size() > 0) {
                long count = sysRolePermissions.stream().map(b -> roles.contains(b.getRoleId())).count();
                if (count > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }).collect(Collectors.toList());
        return result;
    }

    /**
     * 组装菜单
     *
     * @param parentlist
     * @param list
     * @return
     */
    private List<UserMenu> assembleUserMenu(List<UserMenu> parentlist, List<UserMenu> list) {
        parentlist.forEach(a -> {
            List<UserMenu> subs = list.stream().filter(b -> a.getId().equals(b.getParentId())).collect(Collectors.toList());
            if (subs != null && subs.size() > 0) {
                a.setSubs(subs);
            }
        });
        return parentlist;
    }
}
