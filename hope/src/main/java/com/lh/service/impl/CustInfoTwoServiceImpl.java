package com.lh.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lh.dao.sys.CustInfoMapper;
import com.lh.entity.CustInfo;
import com.lh.service.CustInfoService;
import com.lh.service.task.CustQueryTwoThread;
import com.lh.service.task.CustThreadPoolExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

@Slf4j
@Service("custInfoTwoServiceImpl")
public class CustInfoTwoServiceImpl implements CustInfoService {

    private static final int PAGE_SIZE = 30;
    private static final int QUERY_SIZE = 10;

    @Resource
    private CustInfoMapper custInfoMapper;

    @Override
    public void queryJob() {
        try {
            log.info("==============cust query task begin!=================");
            int i = custInfoMapper.selectCustInfoCount();
            int pageCount = i / PAGE_SIZE + (i % PAGE_SIZE > 0 ? 1 : 0);
            int flag = 1;
            while (flag <= pageCount) {
                Page page = PageHelper.startPage(1, PAGE_SIZE);
                List<CustInfo> list = custInfoMapper.selectCustInfoList();
                if (list == null || list.size() == 0) {
                    break;
                }
                this.query(list);
                flag++;
            }
            log.info("==============cust query task end !=================");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("==============cust query task exception! e={}=================", e);
        }
    }

    @Override
    public void testInsert() {

    }

    private void query(List<CustInfo> list) throws InterruptedException {
        int size = list.size();
        int m = size / QUERY_SIZE;
        int n = size % QUERY_SIZE;
        int threadCount = m + (n > 0 ? 1 : 0);
        CountDownLatch latch = new CountDownLatch(threadCount);
        if (m > 0) {
            for (int i = 0; i < m; i++) {
                List<CustInfo> collect = list.stream().skip(QUERY_SIZE * i).limit(QUERY_SIZE).collect(Collectors.toList());
                CustThreadPoolExecutor.execute(new CustQueryTwoThread(collect, latch));
            }
        }
        if (n > 0) {
            List<CustInfo> collect = list.stream().skip(QUERY_SIZE * m).collect(Collectors.toList());
            CustThreadPoolExecutor.execute(new CustQueryTwoThread(collect, latch));
        }
        // 主线程等待所以有子线程执行完毕再继续执行
        latch.await();
    }
}
