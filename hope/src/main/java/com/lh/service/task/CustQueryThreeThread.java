package com.lh.service.task;

import com.alibaba.fastjson.JSON;
import com.lh.dao.sys.CustDetailMapper;
import com.lh.dao.sys.CustInfoMapper;
import com.lh.entity.CustDetail;
import com.lh.entity.CustInfo;
import com.lh.utils.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

@Slf4j
public class CustQueryThreeThread implements Runnable {

    private List<CustInfo> data;

    private Map<String, CustInfo> map;

    private Semaphore semaphore;

    private SqlSessionFactory sqlSessionFactory;

    public CustQueryThreeThread(List<CustInfo> data, Semaphore semaphore) {
        this.data = data;
        this.map = new HashMap<>();
        this.semaphore = semaphore;
        this.sqlSessionFactory = SpringUtil.getBean(SqlSessionFactory.class);
    }

    @Override
    public void run() {
        try {
            // 1、组装参数
            String custIds = this.before();
            // 2、调用接口 我们这里简单模拟
            System.out.println("sleep =====  begin");
            Thread.sleep(10000);
            System.out.println("sleep =====  end");
            String result = this.runing(custIds);
            // 3、解析返回保存记录
            this.after(result);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询客户信息发生异常！e={}", e);
        } finally {
            //线程执行完毕 释放许可
            System.out.println("sleep ===== release ");
            semaphore.release();
        }
    }

    /**
     * 组装接口调用入参
     *
     * @return
     */
    private String before() {
        return data.stream().map(a -> {
            map.put(a.getCustId(), a);
            return a.getCustId();
        }).collect(Collectors.joining(","));
    }

    /**
     * 模拟调用外部接口返回Json 数据
     *
     * @return
     */
    private String runing(String request) {
        List<CustDetail> list = Arrays.stream(request.split(",")).map(a -> {
            CustDetail custDetail = new CustDetail();
            custDetail.setCustId(a);
            custDetail.setAge(12);
            custDetail.setCity("北京");
            custDetail.setTel("17788998880");
            custDetail.setName("小花");
            return custDetail;
        }).collect(Collectors.toList());
        return JSON.toJSONString(list);
    }

    /**
     * 解析接口返回并保存结果
     *
     * @param result
     */
    private void after(String result) {
        SqlSession session = sqlSessionFactory.openSession(ExecutorType.BATCH);
        try {
            CustDetailMapper custDetailMapper = session.getMapper(CustDetailMapper.class);
            CustInfoMapper custInfoMapper = session.getMapper(CustInfoMapper.class);
            List<CustDetail> list = JSON.parseArray(result, CustDetail.class);
            for (CustDetail custDetail : list) {
                custDetailMapper.insertSelective(custDetail);
                CustInfo custInfo = map.get(custDetail.getCustId());
                custInfo.setStatus(Byte.parseByte("1"));
                custInfo.setRemark("处理成功");
                custInfoMapper.updateByPrimaryKeySelective(custInfo);
            }
            session.commit();
            session.clearCache();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存接口返回发生异常！e={}", e);
            session.rollback();
        } finally {
            session.close();
        }
    }

}
