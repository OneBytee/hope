package com.lh.service.task;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class CustThreadPoolExecutor {

    private static ThreadPoolExecutor pool;

    private static void init() {
        pool = new ThreadPoolExecutor(
                3,
                6,
                1000,
                TimeUnit.MICROSECONDS,
                new ArrayBlockingQueue<Runnable>(30)
        );
    }

    private CustThreadPoolExecutor() {
        pool = new ThreadPoolExecutor(
                3,
                6,
                1000,
                TimeUnit.MICROSECONDS,
                new ArrayBlockingQueue<Runnable>(30)
        );
    }

    public static void execute(Runnable runnable) {
        if (pool == null) {
            init();
        }
        pool.execute(runnable);
    }
}
