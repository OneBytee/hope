package com.lh.service;

import com.lh.entity.common.Result;
import com.lh.entity.sys.common.UserMenu;

import java.util.List;

public interface SysService {

    /**
     * 根据用户编号查询用户菜单
     *
     * @param userId
     * @return
     */
    Result<List<UserMenu>> selectUserMenu(String userId);

    /**
     * 根据用户编号查询用户可访问资源
     *
     * @param userId
     * @return
     */
    Result<List<String>> selectUserPermission(String userId);
}
