package com.lh.service;

import com.lh.common.BaseResponse;
import com.lh.entity.sys.SysUser;
import com.lh.entity.common.PageModel;
import com.lh.entity.common.Result;
import com.lh.entity.sys.SysUserDTO;

public interface SysUserService {

    Result<PageModel<SysUser>> selectUserList(SysUser user);

    Result<String> deleteUserById(Integer id);

    Result<String> editUserById(SysUser user);

    Result<String> addUser(SysUser sysUser);

    SysUser selectSysUserByUserId(String userId);

    SysUser selectSysTest(String userId);

    void testBatch();

    BaseResponse<PageModel<SysUser>> queryUserList(SysUserDTO dto);
}
