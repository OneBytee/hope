package com.lh.controller;

import com.lh.entity.sys.SysUser;
import com.lh.entity.common.Result;
import com.lh.common.enums.TransactionCode;
import com.lh.entity.sys.common.UserMenu;
import com.lh.service.SysService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
public class SysController {

    @Resource
    private SysService sysService;

    /**
     * 登录方法
     *
     * @param sysUser
     * @return
     */
    @RequestMapping(value = "/sys/login", method = RequestMethod.POST)
    public Result ajaxLogin(@RequestBody SysUser sysUser) {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(sysUser.getUserId(), sysUser.getPassword());
        try {
            subject.login(token);
            return Result.ok(subject.getSession().getId());
        } catch (IncorrectCredentialsException e) {
            return Result.error("密码错误!");
        } catch (LockedAccountException e) {
            return Result.error("登录失败，该用户已被冻结!");
        } catch (AuthenticationException e) {
            return Result.error("该用户不存在!");
        } catch (Exception e) {
            log.info("用户登录时发生异常！e={}", e);
            return Result.error();
        }
    }

    /**
     * 未登录，shiro应重定向到登录界面，此处返回未登录状态信息由前端控制跳转页面
     *
     * @return
     */
    @RequestMapping(value = "/unauth")
    public Result<String> unauth() {
        return Result.error(TransactionCode.NO_LOGION.getCode(), TransactionCode.NO_LOGION.getMsg());
    }

    /**
     * 根据用户编号查询用户所有菜单
     *
     * @param sysUser
     * @return
     */
    @RequestMapping(value = "/sys/selectUserMenu", method = RequestMethod.POST)
    public Result<List<UserMenu>> selectUserMenu(@RequestBody SysUser sysUser) {
        // 根据用户编号查询用户所有可见菜单
        if (StringUtils.isNotBlank(sysUser.getUserId())) {
            return sysService.selectUserMenu(sysUser.getUserId());
        } else {
            return Result.error("未获取到当前登录人信息！");
        }
    }

    @RequestMapping(value = "/sys/selectUserPermission", method = RequestMethod.POST)
    public Result<List<String>> selectUserPermission(@RequestBody SysUser sysUser) {
        if (StringUtils.isNotBlank(sysUser.getUserId())) {
            return sysService.selectUserPermission(sysUser.getUserId());
        } else {
            return Result.error("未获取到当前登录人信息！");
        }
    }

}
