package com.lh.controller;

import com.alibaba.fastjson.JSON;
import com.lh.common.BaseResponse;
import com.lh.entity.sys.SysUser;
import com.lh.entity.common.PageModel;
import com.lh.entity.common.Result;
import com.lh.entity.sys.SysUserDTO;
import com.lh.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/user")
public class SysUserController {

    @Resource
    private SysUserService sysUserService;

    @RequestMapping(value = "/selectUserList", method = RequestMethod.POST)
    public Result<PageModel<SysUser>> selectUserList(@RequestBody SysUser sysUser) {
        log.info("查询用户列表开始！user={}", JSON.toJSON(sysUser));
        return sysUserService.selectUserList(sysUser);
    }

    @PostMapping("/deleteUserById")
    public Result<String> deleteUserById(@RequestBody SysUser sysUser) {
        log.info("删除用户开始！SysUser={}", sysUser);
        if (sysUser.getId() == null) {
            log.error("要删除的用户编号为空！");
            return Result.error("要删除的用户编号为空！");
        } else {
            return sysUserService.deleteUserById(sysUser.getId());
        }
    }

    @PostMapping("/editUserById")
    public Result<String> editUserById(@RequestBody SysUser sysUser) {
        log.info("编辑用户开始！SysUser={}", sysUser);
        if (sysUser.getId() == null) {
            log.error("要编辑的用户编号为空！");
            return Result.error("要编辑的用户编号为空！");
        } else {
            return sysUserService.editUserById(sysUser);
        }
    }

    @PostMapping("/addUser")
    public Result<String> addUser(@RequestBody SysUser sysUser) {
        log.info("新增用户开始！sysUser={}", sysUser);
        return sysUserService.addUser(sysUser);
    }

    @PostMapping("/selectUserInfoByUserId")
    public Result<SysUser> selectUserInfoByUserId(@RequestBody SysUser sysUser) {
        if (StringUtils.isNotBlank(sysUser.getUserId())) {
            return Result.ok(sysUserService.selectSysUserByUserId(sysUser.getUserId()));
        } else {
            return Result.error("用户编号为空！");
        }
    }

    @PostMapping("/queryUserList")
    public BaseResponse<PageModel<SysUser>> queryUserList(@RequestBody SysUserDTO dto) {
        return sysUserService.queryUserList(dto);
    }
}

