package com.lh.controller;

import com.lh.entity.common.Result;
import com.lh.entity.sys.SysUser;
import com.lh.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloController {

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = "/sayHello", method = RequestMethod.GET)
    public String sayHello() {
        return "hello hope!!!!!!--- ---------";
    }

    @RequestMapping(value = "/testBatch")
    public Result<SysUser> testBatch() {
        return Result.ok(sysUserService.selectSysTest("xiaopang"));
    }

    @RequestMapping(value = "/testSaveBatch")
    public Result testSaveBatch() {
        sysUserService.testBatch();
        return Result.ok();
    }

}
