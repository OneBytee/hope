package com.lh.hope;

import org.junit.Test;

import java.io.IOException;
import java.io.StringBufferInputStream;
import java.util.Properties;

public class HopeTest {

    @Test
    public void test01() throws IOException {
        String str = "druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000";
        Properties properties = new Properties();
        properties.load(new StringBufferInputStream(str));
        System.out.println(properties.get("druid.stat.mergeSql"));
        System.out.println(properties.get("druid.stat.slowSqlMillis"));

    }
}
